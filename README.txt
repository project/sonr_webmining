PoolParty GraphSearch

-- SUMMARY --

The PoolParty GraphSearch automatically collects content from multiple external
sources and presents them in Drupal, filterable by date, geographic region and
extracted tags of the content.

-- REQUIREMENTS --

- A PoolParty-server with the Thesaurus Manager (PPT), the Extractor (PPX) and
the GraphSearch (PPGS) has to be existent. The configuration of the server can
be done inside the module.
- cURL needs to be installed on the web server your Drupal-instance runs on.
- The "PoolParty GraphSearch"-module (https://drupal.org/project/sonr_webmining)
needs to be installed and enabled.
- The "Semantic Connector"-module (https://drupal.org/project/semantic_connector)
needs to be installed and enabled.
- The "Views"-module (https://drupal.org/project/views) and the sub-module
"Views UI" needs to be installed and enabled.
- If you want to show a trends chart of selected tags, the sub-module
"PoolParty GraphSearch Trends" and the "Flot"-module
(https://www.drupal.org/project/flot) should be installed and enabled.

-- INSTALLATION --

Install the "Semantic Connector"-module, the "PoolParty GraphSearch"-module and
"Views"-module as usual.
See https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.
Check, if a PoolParty-server with PPT, PPX and PPGS is running.

-- USAGE --

- Install and enable the Drupal-modules.
- Create at least one configuration set for the PoolParty-server connection at
"admin/config/semantic-drupal/semantic-connector".
- Create at least one configuration set for the PoolParty GraphSearch at
"admin/config/semantic-drupal/sonr-webmining".
- Make sure there is at least one active agent aggregating data for the
PoolParty GraphSearch-configuration you used in the configuration set at
"admin/config/semantic-drupal/sonr-webmining/agents".
- Add the blocks "PoolParty GraphSearch Content" (and "PoolParty GraphSearch
Filters" if available) anywhere on the Drupal-website.
