(function ($) {

  Drupal.behaviors.sonr_webmining = {
    attach: function () {

      var page = 0;
      var loading = false;
      var items_loaded = null;
      var filter_form = $("#sonr-webmining-filter-form");
      var search_bar_form = $("#sonr-webmining-search-bar-form");
      var facet_box_form = $("#sonr-webmining-facet-box-form");
      var filter_operations = $("#filter-operations");
      var result_list = $("div.view-content");

      // Initialize the facet box.
      Drupal.GraphSearchFacetBox.init();

      // Set the submit event for filter form.
      filter_form.once("sonr-webmining", function () {
        $(this).submit(function (event) {
          event.preventDefault();
          page = 0;
          loading = false;

          // Collect all data.
          var data = {filters: collectFilters(false)};

          // Call ajax callback function.
          search(data, page);
        });
      });

      // Set the change event for the filters.
      filter_form.find("input:not(.filter-reset, .facet-autocomplete), select").once("sonr-webmining", function () {
        $(this).change(function () {
          // Remove the count value with the brackets.
          var label = $(this).next().text();
          label = label.slice(0, label.lastIndexOf("("));
          var facet = {
            type: "concept",
            field: $(this).attr("data-field"),
            value: $(this).val(),
            label: label
          };
          if ($(this).is(":checked")) {
            Drupal.GraphSearchFacetBox.add(facet);
          }
          else {
            Drupal.GraphSearchFacetBox.remove(facet);
          }
        });
      });

      // Set the collapsing for the filters.
      filter_form.find(".tree-open-close").once("sonr-webmining", function () {
        $(this).each(function () {
          if ($(this).next().find("input:checked").length > 0) {
            $(this).removeClass('collapsed');
            $(this).next().show();
          }
        });

        $(this).click(function () {
          if ($(this).hasClass('collapsed')) {
            $(this).removeClass('collapsed');
            $(this).next().slideDown(200);
          }
          else {
            $(this).addClass('collapsed');
            $(this).next().slideUp(200);
          }
        });
      });

      // Set click event for filter reset button.
      filter_form.find(".filter-reset").once("sonr-webmining", function () {
        $(this).click(function () {
          return reset();
        });
      });

      // Hide or grey out empty fieldsets.
      filter_form.find(".form-checkboxes").once("sonr-webmining", function () {
        var facet_fieldset = $(this).closest("fieldset");
        var facet_link = facet_fieldset.find("legend span");

        if ($(this).children().length === 0) {
          if (Drupal.settings.sonr_webmining.hide_empty_facet) {
            facet_fieldset.parent().parent().parent().slideUp(300);
          }
          else {
            facet_link.addClass("greyed-out");
          }
        }
      });

      // Set the change event for the filters.
      filter_form.find("input.facet-autocomplete").once("sonr-webmining", function () {
        var input = $(this);
        input.focus(function () {
          input.val("");
        });
        input.autocomplete({
          minLength: Drupal.settings.sonr_webmining.min_chars,
          source: function (request, response) {
            var url = Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/autocomplete/" + getConfigurationSetId() + "/" + Drupal.settings.sonr_webmining.max_suggestions + "/" + input.attr("data-field");
            $.getJSON(url, {term: request.term}, function (data) {
              input.removeClass("throbbing");
              response(data);
            });
          },
          focus: function (event, ui) {
            this.value = ui.item.label;
            return false;
          },
          select: function (event, ui) {
            this.value = ui.item.label;
            Drupal.GraphSearchFacetBox.setCache({
              type: "concept",
              field: ui.item.field,
              value: ui.item.value,
              label: ui.item.label
            });
            search_bar_form.find(".search-bar-submit").click();
            return false;
          }
        });

        // Set the custom popup menu for the autocomplete field.
        var autocomplete = input.data("ui-autocomplete");
        var data_item = "ui-autocomplete-item";
        if (typeof autocomplete === "undefined") {
          autocomplete = input.data("autocomplete");
          data_item = "item.autocomplete";
        }

        autocomplete._renderItem = function (ul, item_data) {
          var item = $("<a>" + item_data.label + "</a>");
          if (Drupal.settings.sonr_webmining.add_matching_label && item_data.matching_label.length > 0) {
            $("<div class='ui-menu-item-metadata'>")
                .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Matching label") + ":</span>")
                .append("<span class='ui-menu-item-metadata-value'>" + item_data.matching_label + "</span>")
                .appendTo(item);
          }
          if (Drupal.settings.sonr_webmining.add_context && item_data.context.length > 0) {
            $("<div class='ui-menu-item-metadata'>")
                .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Context") + ":</span>")
                .append("<span class='ui-menu-item-metadata-value'>" + item_data.context + "</span>")
                .appendTo(item);
          }

          return $("<li>").data(data_item, item_data).append(item).appendTo(ul);
        };
      });

      // Set click event for search bar submit button.
      search_bar_form.find(".search-bar-submit").once("sonr-webmining", function () {
        $(this).click(function (event) {
          event.preventDefault();
          if (Drupal.GraphSearchFacetBox.isEmptyCache()) {
            var value = search_bar_form.find("input.form-text").val();
            var facet = {
              type: "free-term",
              field: "dyn_txt_content",
              value: value.trim(),
              label: value.trim()
            };
          }
          else {
            facet = Drupal.GraphSearchFacetBox.getCache();
            Drupal.GraphSearchFacetBox.clearCache();
          }
          if (facet.label !== "") {
            Drupal.GraphSearchFacetBox.add(facet);
          }
        });
      });

      // Set click event for search bar reset button.
      search_bar_form.find(".search-bar-reset").once("sonr-webmining", function () {
        $(this).click(function () {
          return reset();
        });
      });

      // Set focus event for the search bar text field.
      search_bar_form.find("input.form-text").once("sonr-webmining", function () {
        var input = $(this);
        input.focus(function () {
          input.val("");
        });
        input.autocomplete({
          minLength: Drupal.settings.sonr_webmining.min_chars,
          source: function (request, response) {
            var url = Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/autocomplete/" + getConfigurationSetId() + "/" + Drupal.settings.sonr_webmining.max_suggestions;
            $.getJSON(url, {term: request.term}, function (data) {
              input.removeClass("throbbing");
              response(data);
            });
          },
          search: function (event, ui) {
            input.addClass("throbbing");
          },
          focus: function (event, ui) {
            this.value = ui.item.label;
            return false;
          },
          select: function (event, ui) {
            this.value = ui.item.label;
            Drupal.GraphSearchFacetBox.setCache({
              type: "concept",
              field: ui.item.field,
              value: ui.item.value,
              label: ui.item.label
            });
            search_bar_form.find(".search-bar-submit").click();
            return false;
          }
        });

        // Set the custom popup menu for the autocomplete field.
        var autocomplete = input.data("ui-autocomplete");
        var data_item = "ui-autocomplete-item";
        if (typeof autocomplete === "undefined") {
          autocomplete = input.data("autocomplete");
          data_item = "item.autocomplete";
        }

        autocomplete._renderItem = function (ul, item_data) {
          var item = $("<a>" + item_data.label + "</a>");
          if (Drupal.settings.sonr_webmining.add_matching_label && item_data.matching_label.length > 0) {
            $("<div class='ui-menu-item-metadata'>")
                .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Matching label") + ":</span>")
                .append("<span class='ui-menu-item-metadata-value'>" + item_data.matching_label + "</span>")
                .appendTo(item);
          }
          if (Drupal.settings.sonr_webmining.add_context && item_data.context.length > 0) {
            $("<div class='ui-menu-item-metadata'>")
                .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Context") + ":</span>")
                .append("<span class='ui-menu-item-metadata-value'>" + item_data.context + "</span>")
                .appendTo(item);
          }

          return $("<li>").data(data_item, item_data).append(item).appendTo(ul);
        };
      });

      // Set the submit event for facet box form.
      facet_box_form.once("sonr-webmining", function () {
        $(this).submit(function (event) {
          event.preventDefault();
          loading = false;

          // Collect all data.
          var data = {
            filters: collectFilters(true),
            title: facet_box_form.find('#edit-title').val(),
            time_interval: facet_box_form.find('#edit-time-interval').val()
          };

          // Call ajax callback function.
          saveFilter(data);
        });
      });

      // Set click event for facet box form save button.
      facet_box_form.find(".facet-box-form-save").once("sonr-webmining", function () {
        $(this).click(function (event) {
          event.preventDefault();
          Drupal.GraphSearchFacetBox.submitFilterForm();
        });
      });

      // Set click event for search bar save button.
      filter_operations.find(".add").once("sonr-webmining", function () {
        $(this).click(function (event) {
          Drupal.GraphSearchFacetBox.triggerFilterForm();
        });
      });

      filter_operations.find(".show").once("sonr-webmining", function () {
        $(this).click(function (event) {
          if (Drupal.GraphSearchFacetBox.filters().is(":hidden")) {
            showFilters();
          }
          else {
            Drupal.GraphSearchFacetBox.hideFilters();
          }
        });
      });

      // Set click event for similar documents link.
      result_list.find(".similar-more").once("sonr-webmining", function () {
        var uri = $(this).attr('data-uri');
        $(this).click(function () {
          var container = $(this).next();

          // If the container is empty, then get similars
          // else open or close the container
          if (container.html() === "") {
            getSimilars(uri, $(this).next());
          }
          else {
            if (container.is(':visible')) {
              container.slideUp();
            }
            else {
              container.slideDown();
            }
          }
        });
      });

      // Set click event for "show all tags"
      result_list.find(".tags-more").once("sonr-webmining", function () {
        var tags_more = $(this);
        $(this).parent().click(function () {
          tags_more.addClass("element-hidden");
          $(this).find(".tags-rest").fadeIn();
        });
      });

      // Add the trend chart if data is available
      if ($("#edit-trends").children().length) {
        plotData("trends-placeholder", $.parseJSON($("#edit-trends").find(".float-chart-values").text()));
        useTooltip("trends-placeholder");
      }

      // Set the infinity scroll event.
      $("body").once("sonr-webmining", function () {
        if ($("div.view-sonr-webmining-list").hasClass("views-load-more")) {
          $(window).scroll(function () {
            var winTop = $(window).scrollTop(),
                winHeight = $(window).height(),
                docHeight = $(document).height(),
                footerHeight = $("#" +  Drupal.settings.sonr_webmining.footer_id).height(),
                scrollTrigger = 1;

            if (items_loaded === null) {
              items_loaded = $(".view-sonr-webmining-list>div.view-content").children().length;
            }

            if ((items_loaded >= Drupal.settings.sonr_webmining.items_per_page) && ((winTop / (docHeight - winHeight - footerHeight)) >= scrollTrigger)) {
              loadInfinity();
            }
          });
        }
      });

      // Starts the search with the given filters.
      var search = function (data, page) {
        var url = Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/get-results/" + getConfigurationSetId() + "/" + page;
        $.getJSON(url, data, function (data) {
          // Show result list.
          var content_block = $(".view-sonr-webmining-list").closest(".block-sonr-webmining");
          content_block.children("div").remove();
          content_block.append(data.list);
          items_loaded = $(data.list).find("div.view-content").children().length;
          Drupal.GraphSearchFacetBox.showAll(false);

          // Show the new filter.
          var new_filter_form = $(data.filter);
          new_filter_form.find(".form-checkboxes").each(function () {
            var facet_id = this.id.substr(5);
            var facet_fieldset = $("#edit-sw-fieldset-" + facet_id);
            var facet_link = facet_fieldset.find("a.fieldset-title");
            var checkboxes = $("#edit-" + facet_id);
            checkboxes.empty().append($(this).html());

            if ($(this).children().length === 0) {
              if (Drupal.settings.sonr_webmining.hide_empty_facet) {
                facet_fieldset.parent().parent().parent().slideUp(300);
              }
              else {
                facet_link.parent().addClass("greyed-out");
                facet_link.unbind("click").click(function () {
                  return false;
                });
              }
            }
            else {
              if (Drupal.settings.sonr_webmining.hide_empty_facet) {
                facet_fieldset.parent().parent().parent().slideDown(300);
              }
              else {
                facet_link.parent().removeClass("greyed-out");
                facet_link.unbind("click").click(function () {
                  var fieldset = facet_fieldset.get(0);
                  // Don't animate multiple times.
                  if (!fieldset.animating) {
                    fieldset.animating = true;
                    Drupal.toggleFieldset(fieldset);
                  }
                  return false;
                });
              }
              if (typeof(Drupal.behaviors.uniform) !== "undefined") {
                checkboxes.find("input").uniform();
              }
              if (checkboxes.find("input:checked").length > 0 && facet_fieldset.hasClass("collapsed")) {
                facet_fieldset.find("a.fieldset-title").trigger("click");
              }
            }
          });

          // Add the date values
          var from_date = new_filter_form.find("#edit-date-from input").val();
          $("#edit-date-from").find("input").val(from_date);
          var to_date = new_filter_form.find("#edit-date-to input").val();
          $("#edit-date-to").find("input").val(to_date);

          // Add the trends-chart if enabled.
          if (new_filter_form.find("#edit-trends-wrapper").length > 0) {
            var trends = $("#edit-trends-wrapper");
            trends.empty();
            if (new_filter_form.find("#edit-trends-wrapper").children().length > 0) {
              trends.append(new_filter_form.find("#edit-trends-wrapper").html());
            }
          }
          Drupal.behaviors.sonr_webmining.attach();
          Drupal.behaviors.semanticConnectorConceptDestinations.attach();

          filter_form.find(".tree-open-close").each(function () {
            if ($(this).next().find("input:checked").length > 0) {
              $(this).removeClass('collapsed');
              $(this).next().show();
            }
          });
        });
      };

      // Saves the filters for the logged in user.
      var saveFilter = function (data) {
        $.get(Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/search-filters/save/" + getConfigurationSetId(), data, function (data) {
          if (data === 'saved') {
            Drupal.GraphSearchFacetBox.hideFilterForm();
          }
          else {
            facet_box_form.find(".error").html(data).slideDown();
          }
        });
      };

      // Shows all the saved filters for the logged in user as a list.
      var showFilters = function () {
        $.get(Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/search-filters/get/" + getConfigurationSetId(), function (data) {
          Drupal.GraphSearchFacetBox.showFilters(data);
          facet_box_form.find(".search-filter-title").each(function () {
            $(this).bind("click", function () {
              searchFilter(this);
            });
          });
          facet_box_form.find(".search-filter-remove").each(function () {
            $(this).click(function () {
              removeFilter(this);
            });
          });
        });
      };

      // Starts a search from a saved filter in the filter list.
      var searchFilter = function (item) {
        // Reset the all filters.
        Drupal.GraphSearchFacetBox.hideAll(false);
        Drupal.GraphSearchFacetBox.facets = [];
        filter_form.find('input[name="date_from[date]"]').val("");
        filter_form.find('input[name="date_to[date]"]').val("");

        var filters = $(item).data("filter");
        $.each(filters.filters, function (index, facet) {
          if (facet.field === "date-from" || facet.field === "date-to") {
            filter_form.find('input[data-field="' + facet.field + '"]').val(facet.value);
          }
          else {
            Drupal.GraphSearchFacetBox.facets.push(facet);
          }
        });
        Drupal.GraphSearchFacetBox.showAll(true);
        Drupal.GraphSearchFacetBox.hideFilters();
        Drupal.GraphSearchFacetBox.hideFilterForm();

        var data = {filters: collectFilters(false)};
        search(data, 0);
      };

      // Deletes a saved filter from the logged in user.
      var removeFilter = function (item) {
        var data = {sfid: $(item).data("sfid")};
        $.get(Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/search-filters/delete/" + getConfigurationSetId(), data, function (data) {
          if (data === "deleted") {
            $(item).parent().fadeOut(function () {
              $(this).remove();
            });
            if ($(item).parent().siblings().length === 0) {
              Drupal.GraphSearchFacetBox.hideFilters();
            }
          }
          else {
            $(item).html(data).addClass("messages error");
          }
        });
      };

      // Gets the similar documents from a given document URI.
      var getSimilars = function (uri, container) {
        $.get(Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/get-similars/" + getConfigurationSetId(), {uri: uri}, function (data) {
          $(container).hide().html(data).slideDown();
          Drupal.behaviors.sonr_webmining.attach(data);
          Drupal.behaviors.semanticConnectorConceptDestinations.attach();
        });
      };

      // The load infinity function.
      var loadInfinity = function () {
        if (!loading) {
          loading = true;

          // Collect all data and increase page number.
          page += 1;
          var data = {
            filters: collectFilters(false)
          };

          // Call ajax callback function.
          var url = Drupal.settings.basePath + Drupal.settings.pathPrefix + "sonr-webmining/get-results/" + getConfigurationSetId() + "/" + page;
          $.getJSON(url, data, function (data) {
            // Add new list items.
            items_loaded = $(data.list).find("div.view-content").children().length;
            if (items_loaded > 0) {
              $('.view-sonr-webmining-list>div.view-content').append($(data.list).find('div.view-content').html());
              loading = false;
              Drupal.behaviors.sonr_webmining.attach(data);
              Drupal.behaviors.semanticConnectorConceptDestinations.attach();
            }
          });
        }
      };

      // The reset function.
      var reset = function () {
        filter_form.find('input[name="date_from[date]"]').val("");
        filter_form.find('input[name="date_to[date]"]').val("");
        filter_form.find('select[name="date_from[date]"]').val("");
        Drupal.GraphSearchFacetBox.clear();
        return false;
      };

      // Collects the filter data.
      var collectFilters = function (withLabel) {
        // Collect the selected filters.
        var $form = $("#sonr-webmining-filter-form"), filters = [];

        // Get all selected facets (concepts and free terms)
        Drupal.GraphSearchFacetBox.facets.forEach(function (facet) {
          if (withLabel) {
            filters.push({
              field: facet.field,
              value: facet.value,
              label: facet.label,
              type: facet.type
            });
          }
          else {
            filters.push({field: facet.field, value: facet.value});
          }
        });

        // From - To text fields
        $form.find('input[name="date_from[date]"], input[name="date_to[date]"]').each(function () {
          if ($(this).val().length) {
            filters.push({
              field: $(this).attr("data-field"),
              value: $(this).val()
            });
          }
        });
        // Select date format
        $form.find('select[name="date_from[date]"]').each(function () {
          var date_length = $(this).val().length;
          if (date_length) {
            if (date_length > 4) {
              filters.push({
                field: $(this).attr("data-field"),
                value: $(this).val()
              });
            }
            else {
              filters.push({
                field: $(this).attr("data-field"),
                value: $(this).val() + "-01-01"
              });
              filters.push({field: "date-to", value: $(this).val() + "-12-31"});
            }
          }
        });

        return filters;
      };

      // Gets the PoolParty GraphSearch configuration set id from the DOM
      var getConfigurationSetId = function () {
        return $('.view-sonr-webmining-list').parent().attr('data-id');
      };

      // Insert a clear:both style after right area
      $(".block-sonr-webmining .sonr-webmining-area-right").once("sonr-webmining", function () {
        $('<div style="clear:both;" />').insertAfter($(this));
      });

      // Exposed filter adaptions
      $(".block-sonr-webmining").find(".views-exposed-form").find(".views-widget-filter-date-from").once("sonr-webmining", function () {
        $(this).prepend($(this).find("label"));
      });

    }
  };

  Drupal.GraphSearchFacetBox = {
    facets: [],
    cache: {},

    container: function () {
      return $("#facet-container");
    },

    box: function () {
      return this.container().children(".facet-box").first();
    },

    form: function () {
      return this.container().children(".facet-box-form").first();
    },

    filters: function () {
      return this.container().children(".search-filters").first();
    },

    operations: function () {
      return $("#filter-operations");
    },

    emptybox: function () {
      return this.container().children(".facets-empty").first();
    },

    /* Initializes the facetBox on start up. */
    init: function () {
      if (Drupal.settings.sonr_webmining.hasOwnProperty("facet_box") && Drupal.settings.sonr_webmining.facet_box.length) {
        this.facets = [];
        Drupal.settings.sonr_webmining.facet_box.forEach(function (facet) {
          Drupal.GraphSearchFacetBox.facets.push(facet);
          Drupal.GraphSearchFacetBox.show(facet, false);
        });
        Drupal.settings.sonr_webmining.facet_box = [];
      }
      if (this.facets.length > 0) {
        this.operations().children(".add").show();
        this.emptybox().hide();
      }
      else {
        this.emptybox().show();
      }
    },

    /* Adds a new facet to the facetBox. */
    add: function (facet) {
      facet.label = facet.label.trim();
      var search_types = Drupal.settings.sonr_webmining.search_type.split(" ");
      if (!this.inFacetBox(facet, this.facets) && $.inArray(facet.type, search_types) !== -1) {
        this.facets.push(facet);
        this.show(facet, true);
        this.submit();
      }
    },

    /* Removes a facet from the facetBox. */
    remove: function (facet) {
      var item;
      if ($.type(facet.field) !== "undefined") {
        item = this.box().find('.' + facet.type + '[data-value="' + facet.value + '"]');
      }
      else {
        item = facet;
        facet = {
          type: item.hasClass("concept") ? 'concept' : 'free-term',
          field: item.data("field"),
          value: item.data("value"),
          label: item.children().first().text()
        };
      }
      this.facets = this.removeFromFacetBox(facet, this.facets);
      this.hide(item, true);
      this.submit();
    },

    /* Clears the facetBox. */
    clear: function () {
      this.facets = [];
      this.box().children().each(function () {
        Drupal.GraphSearchFacetBox.hide($(this), true);
      });
      this.submit();
    },

    /* Shows a facet in the facetBox. */
    show: function (facet, animate) {
      var item_label = $("<div>").addClass("facet-label").html(facet.label);
      var item = $("<div>").addClass("facet-item " + facet.type).click(function () {
        Drupal.GraphSearchFacetBox.remove($(this));
      });
      item.attr("data-value", facet.value);
      item.attr("data-field", facet.field);
      item.append(item_label);
      this.box().append(item);
      if (animate) {
        // Switch from 0 to 1 facet.
        if (this.facets.length === 1) {
          this.emptybox().hide();
          this.operations().children(".add").fadeIn();
        }
        item.hide().fadeIn();
      }
      else {
        // Switch from 0 to 1 facet.
        if (this.facets.length === 1) {
          this.emptybox().hide();
          this.operations().children(".add").show();
        }
      }
    },

    /* Hides a facet in the facetBox. */
    hide: function (facet, animate) {
      var item;
      if ($.type(facet.field) !== "undefined") {
        item = this.box().find('.' + facet.type + '[data-value="' + facet.value + '"]');
      }
      else {
        item = facet;
      }
      if (animate) {
        item.fadeOut(function () {
          // Removing the last facet.
          if (Drupal.GraphSearchFacetBox.facets.length === 0) {
            Drupal.GraphSearchFacetBox.emptybox().show();
          }
          this.remove();
        });
        if (Drupal.GraphSearchFacetBox.facets.length === 0) {
          this.operations().children(".add").fadeOut();
        }
      }
      else {
        // Removing the last facet.
        if (this.facets.length === 0) {
          this.emptybox().show();
          this.operations().children(".add").hide();
        }
        item.remove();
      }
    },

    /* Shows all facets in the facetBox. */
    showAll: function (animate) {
      this.facets.forEach(function (facet) {
        Drupal.GraphSearchFacetBox.show(facet, animate);
      });
    },

    /* Hides all facet in the facetBox. */
    hideAll: function (animate) {
      this.facets.forEach(function (facet) {
        Drupal.GraphSearchFacetBox.hide(facet, animate);
      })
    },

    /* Starts the search with the selected filters. */
    submit: function () {
      this.hideFilterForm();
      this.hideFilters();
      $("#sonr-webmining-filter-form").submit();
    },

    /* Saves one facet into the cache. */
    setCache: function (facet) {
      this.cache = facet;
    },

    /* Returns the cached facet. */
    getCache: function () {
      return this.cache;
    },

    /* Clears the cache. */
    clearCache: function () {
      this.cache = {};
    },

    /* Returns true if the cache is empty otherwise false. */
    isEmptyCache: function () {
      return $.isEmptyObject(this.cache);
    },

    triggerFilterForm: function () {
      if (this.form().is(":hidden")) {
        this.showFilterForm();
      }
      else {
        this.hideFilterForm();
      }
    },

    showFilterForm: function () {
      this.form().slideDown();
      this.operations().children(".add").html(Drupal.t("Close form"));
    },

    hideFilterForm: function () {
      this.form().slideUp(function () {
        $(this).find(".error").html("").hide();
      });
      this.operations().children(".add").html(Drupal.t("Add to my filters"));
    },

    submitFilterForm: function () {
      $("#sonr-webmining-facet-box-form").submit();
    },

    showFilters: function (filters) {
      this.filters().html(filters);
      this.filters().slideDown();
      this.operations().children(".show").html(Drupal.t("Close my filters"));
    },

    hideFilters: function () {
      this.filters().slideUp();
      this.operations().children(".show").html(Drupal.t("Show my filters"));
    },

    inFacetBox: function (facet, facets) {
      var arr = $.grep(facets, function (item) {
        return (item.type === facet.type && item.value === facet.value);
      });
      return arr.length;
    },

    removeFromFacetBox: function (facet, facets) {
      return $.grep(facets, function (item) {
        return (item.type !== facet.type || item.value !== facet.value);
      });
    }

  };
})(jQuery);
