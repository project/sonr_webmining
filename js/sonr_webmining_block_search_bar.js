/**
 * @file
 *
 * Javascript functionalities for the search bar in a block.
 */
(function ($) {

  Drupal.behaviors.sonr_webmining_block_search_bar = {
    attach: function (context, settings) {

      $("div.sonr-webmining-block-search-bar", context).once('sonr_webmining_block_search_bar', function () {
        var input = $("input", this);

        // Search for free terms.
        var search_types = input.data("search-type").split(" ");
        $(this).closest("form").submit(function (event) {
          // Check if searching for free terms is activated.
          if ($.inArray("free-term", search_types) !== -1) {
            window.location.href = Drupal.settings.basePath + input.data("page-path") + '?search=' + input.val();
          }
          event.preventDefault();
        });

        // Clear the input field on focus.
        input.focus(function () {
          input.val("");
        });

        // Add the autocomplete functionality to the input field.
        input.autocomplete({
          minLength: input.data("min-chars"),
          source: function (request, response) {
            var url = Drupal.settings.basePath + input.data("autocomplete-path");
            $.getJSON(url, {term: request.term}, function (data) {
              input.removeClass("throbbing");
              response(data);
            });
          },
          search: function (event, ui) {
            input.addClass("throbbing");
          },
          focus: function (event, ui) {
            this.value = ui.item.label;
            return false;
          },
          select: function (event, ui) {
            this.value = ui.item.label;
            window.location.href = Drupal.settings.basePath + input.data("page-path") + '?uri=' + ui.item.value;
            return false;
          }
        });

        // Set the custom popup menu for the autocomplete field.
        var autocomplete = input.data("ui-autocomplete");
        var data_item = "ui-autocomplete-item";
        if (typeof autocomplete === "undefined") {
          autocomplete = input.data("autocomplete");
          data_item = "item.autocomplete";
        }

        autocomplete._renderItem = function (ul, item_data) {
          var item = $("<a>" + item_data.label + "</a>");
          if (input.data("add_matching_label")) {
            $("<div class='ui-menu-item-metadata'>")
              .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Matching label") + ":</span>")
              .append("<span class='ui-menu-item-metadata-value'>" + item_data.matching_label + "</span>")
              .appendTo(item);
          }
          if (input.data("add_context")) {
            $("<div class='ui-menu-item-metadata'>")
              .append("<span class='ui-menu-item-metadata-label'>" + Drupal.t("Context") + ":</span>")
              .append("<span class='ui-menu-item-metadata-value'>" + item_data.context + "</span>")
              .appendTo(item);
          }

          return $("<li>").data(data_item, item_data).append(item).appendTo(ul);
        };
      });
    }
  };

})(jQuery);
