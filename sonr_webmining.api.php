<?php

/**
 * @file
 * API documentation for PoolParty GraphSearch module.
 */

/**
 * Alter the custom attributes used in a PoolParty GraphSearch search.
 *
 * If any additional custom attributes were added on a PoolParty GraphSearch
 * server, you can add them here to the list of information to fetch from the
 * server and add them in hook_sonr_webmining_list_item_alter later.
 *
 * @param array $custom_attributes
 *   Array of strings (e.g. "dyn_lit_source").
 * @param object $sonr_webmining
 *   An instance of the "SonrWebmining"-class, that makes it possible for you to
 *   check the ID of the PoolParty GraphSearch configuration, its config or
 *   anything else (even use the sOnr API).
 *
 * @see SonrWebmining::search()
 */
function hook_sonr_webmining_custom_attributes_alter(&$custom_attributes, $sonr_webmining) {
  if ($sonr_webmining->getId() == 1) {
    $custom_attributes[] = 'dyn_lit_new_attribute';
  }
}

/**
 * Replace the GraphSearch API function "search" with an other function.
 *
 * @param SemanticConnectorSonrApi $sonr_api
 *   The object to communicate with the GraphSearch server.
 * @param string $search_space_id
 *   The search space to use for the search.
 * @param array $facets
 *   A list of facet objects that should be used for faceting the
 *   search.
 * @param array $filters
 *   A list of filter object parameters that define the query.
 * @param array $parameters
 *   A list of key value pairs
 *
 * @return boolean|array
 *   List of result items or FALSE in case of an error
 *
 * @see SonrWebmining::search()
 * @see SemanticConnectorSonrApi::search()
 */
function hook_sonr_webmining_api_search(SemanticConnectorSonrApi $sonr_api, $search_space_id, array $facets, array $filters, array $parameters) {
}

/**
 * Alter a single item of the search result.
 *
 * With this hook you can add previously fetched data for custom attributes or
 * alter any of the values for one of the default attributes.
 *
 * @param array $item
 *   Array of properties of a single item of a PoolParty GraphSearch search
 *   result.
 * @param array $config
 *   The PoolParty GraphSearch configuration.
 * @param object $sonr_api
 *   An instance of the "SemanticConnectorSonrApi"-class, in case any additional
 *   operation on a single search results item is required.
 *
 * @see SonrWebmining::themeAsView()
 */
function hook_sonr_webmining_list_item_alter(&$item, $config, $sonr_api) {
}
