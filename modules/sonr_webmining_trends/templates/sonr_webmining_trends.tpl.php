<?php

/**
 * @file
 * The template-file for PoolParty GraphSearch trends displayed as a chart.
 *
 * Variables:
 * $data - String of trends-data in json format to be displayed on the chart.
 * $config - Array of chart configuration (not yet manipulated for plot)
 */

?>
<div class="sonr-webmining-trends-block">
  <?php if (!empty($config['title'])): ?><label class="sonr-webmining-trends-title"><?php print $config['title']; ?></label><?php endif; ?>
  <?php if (!empty($config['description'])): ?><p class="sonr-webmining-trends-description">
  <?php print str_replace(array("\r\n", "\r", "\n"), '<br />', $config['description']); ?>
  </p><?php endif; ?>
  <div class="flot-chart">
    <div class="flot-content" id="trends-placeholder" style="height:<?php print $config['height']; ?>; width:<?php print $config['width']; ?>"></div>
    <div id="flot-chart-legend" class="clearBoth"></div>
    <div class="float-chart-values" style="display:none;"><?php print $data; ?></div>
  </div>
</div>
