<?php

/**
 * @file
 * Administrative functions used for the PoolParty GraphSearch Trends module.
 */

/**
 * Administration settings for the PoolParty GraphSearch Trends module configuration.
 */
function sonr_webmining_trends_admin_settings($form, &$form_state) {
  $form['sonr_webmining_trends_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title above the trends-chart'),
    '#maxlength' => 100,
    '#default_value' => variable_get('sonr_webmining_trends_title', 'Trends'),
  );

  $form['sonr_webmining_trends_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('An additional description between the title and the trends-chart.'),
    '#default_value' => variable_get('sonr_webmining_trends_description', ''),
  );

  $form['sonr_webmining_trends_chart_type'] = array(
    '#type' => 'select',
    '#title' => t('Calculation'),
    '#description' => t('Select the type for the calculation for the lines.'),
    '#default_value' => variable_get('sonr_webmining_trends_chart_type', 'Simple moving average'),
    '#options' => drupal_map_assoc(array('Raw data', 'Simple moving average')),
  );

  $form['sonr_webmining_trends_colors'] = array(
    '#type' => 'textfield',
    '#title' => t('Colors'),
    '#description' => t('A comma separated list of colors for the lines in the chart (e.g. #00FF00, red, #0000FF).'),
    '#default_value' => variable_get('sonr_webmining_trends_colors', ''),
  );

  return system_settings_form($form);
}
