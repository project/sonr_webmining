<?php

/**
 * @file
 * The class used to display the PoolParty GraphSearch trends.
 */

class SonrWebminingTrends {

  protected $sonrApi;
  protected $uris;
  protected $searchSpace;
  protected $trends;

  /**
   * Constructor of the SonrWebminingTrends-class.
   *
   * @param object $sonr_api
   *   Object of the PoolParty GraphSearch server
   * @param array $uris
   *   Array of uris to get trends for.
   * @param string $search_space
   *   The search space to use for the trends.
   */
  public function __construct($sonr_api, $uris, $search_space = '') {
    $this->sonrApi = $sonr_api;
    $this->uris = $uris;
    $this->trends = $this->sonrApi->getTrends($uris, $search_space);
  }

  /**
   * Getter-function for the trends-variable.
   *
   * @return array
   *   Array of trends per uri
   */
  public function getTrends() {
    return $this->trends;
  }

  /**
   * Getter-function for the searchSpace-variable.
   *
   * @return string
   *   The ID of the search space
   */
  public function getSearchSpace() {
    return $this->searchSpace;
  }

  /**
   * Get the default config for a display-type.
   *
   * @param string $type
   *   How to display the trends. Possibilities: "chart"
   *
   * @return array
   *   Array of configuration parameters for the display type
   */
  protected function getDefaultConfig($type) {
    switch ($type) {
      case 'chart':
        $config = array(
          'title' => variable_get('sonr_webmining_trends_title', 'Trends'),
          'description' => variable_get('sonr_webmining_trends_description', ''),
          'width' => '600px',
          'height' => '300px',
          'daysToDisplay' => 95,
          'type' => variable_get('sonr_webmining_trends_chart_type', 'Simple moving average'),
          'colors' => variable_get('sonr_webmining_trends_colors', ''),
        );
        break;

      default:
        $config = array();
        break;
    }

    return $config;
  }

  /**
   * Display the trends in any way.
   *
   * @param string $type
   *   How to display the trends. Possibilities: "chart"
   * @param array $config
   *   Array of configuration parameters depending on the type
   *
   * @return string
   *   The rendered HTML-code of the trends-visualization
   */
  public function theme($type = 'chart', $config = array()) {
    $output = '';
    $config = array_merge($this->getDefaultConfig($type), $config);
    $colors = !empty($config['colors']) ? explode(',', $config['colors']): array();

    if (!empty($this->trends)) {
      switch ($type) {
        case 'chart':
          $data = array();
          $start_time = (time() - $config['daysToDisplay'] * 60 * 60 * 24) * 1000;
          foreach ($this->trends as $trend) {
            $uri = $trend['concept']['uri'];
            $data[$uri] = array();
            $data[$uri]['label'] = (isset($trend['concept']['label']) ? $trend['concept']['label'] : $trend['concept']['prefLabel']);
            foreach ($trend['trend'] as $value_set) {
              if ($value_set[0] > $start_time) {
                $data[$uri]['trend'][] = array('time' => (string) $value_set[0], 'value' => $value_set[1]);
              }
            }
          }
          $data_json = '';

          switch ($config['type']) {
            case 'Simple moving average':
              $data_json .= '[';
              $uri_count = 0;
              foreach ($data as $uri => $trend) {
                if ($uri_count != 0) {
                  $data_json .= ', ';
                }
                $data_json .= '{"label": "' . $trend['label'] . '", ';
                if (isset($colors[$uri_count])) {
                  $data_json .= '"color": "' . trim($colors[$uri_count]) . '", ';
                }
                $data_json .= '"data": [';
                $value_count = 0;
                $k = 9;
                $kk = ($k-1)/2;
                if (isset($trend['trend']) && !empty($trend['trend'])) {
                  $c = count($trend['trend']) - 1;
                  for ($t=$kk; $t<=$c-$kk; $t++) {
                    if ($value_count != 0) {
                      $data_json .= ', ';
                    }
                    $sum = 0;
                    for ($s=$t-$kk; $s<=$t+$kk; $s++) {
                      $sum += $trend['trend'][$s]['value'];
                    }
                    $data_json .= '[' . $trend['trend'][$t]['time'] . ', ' . $sum/$k . ']';
                    $value_count++;
                  }
                }
                else {
                  $data_json .= '[' . strtotime('-3 month +' . $kk . ' days') . '000, 0], [' . strtotime('-' . $kk . ' days') . '000, 0]';
                }
                $data_json .= ']}';
                $uri_count++;
              }
              $data_json .= ']';
              break;

            case 'Raw data':
              $data_json .= '[';
              $uri_count = 0;
              foreach ($data as $uri => $trend) {
                if ($uri_count != 0) {
                  $data_json .= ', ';
                }
                $data_json .= '{"label": "' . $trend['label'] . '", ';
                if (isset($colors[$uri_count])) {
                  $data_json .= '"color": "' . trim($colors[$uri_count]) . '", ';
                }
                $data_json .= '"data": [';
                $value_count = 0;
                if (isset($trend['trend']) && !empty($trend['trend'])) {
                  foreach ($trend['trend'] as $value_set) {
                    if ($value_count != 0) {
                      $data_json .= ', ';
                    }
                    $data_json .= '[' . $value_set['time'] . ', ' . $value_set['value'] . ']';
                    $value_count++;
                  }
                }
                else {
                  $data_json .= '[' . strtotime('-3 month') . '000, 0], [' . time() . '000, 0]';
                }
                $data_json .= ']}';
                $uri_count++;
              }
              $data_json .= ']';
              break;
          }
          $output = theme('sonr_webmining_trends', array('data' => $data_json, 'config' => $config));
          break;
      }
    }

    return $output;
  }
}
