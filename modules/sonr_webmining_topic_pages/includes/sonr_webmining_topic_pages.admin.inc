<?php

/**
 * @file
 * Administrative functions used for the PoolParty GraphSearch Topic Pages module.
 */

function sonr_webmining_topic_pages_list() {
  $output = array();

  $output['topic_pages_title'] = array(
    '#type' => 'markup',
    '#markup' => '<h3 class="semantic-connector-table-title">' . t('PoolParty GraphSearch topic pages') . '</h3>',
  );

  $rows = array();
  $config_sets = sonr_webmining_topic_page_config_load_multiple();
  foreach ($config_sets as $config) {
    $actions = array(
      l(t('Edit'), 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $config->getId()),
      l(t('Clone'), 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $config->getId() . '/clone'),
      l(t('Delete'), 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $config->getId() . '/delete'),
    );

    // Get the project label.
    $project_id = $config->getProjectID();
    $connection_config = $config->getConnection()->getConfig();
    $project_label = 'project label not found';
    if (isset($connection_config['projects'])) {
      foreach ($connection_config['projects'] as $project) {
        if ($project->id == $project_id) {
          $project_label = $project->title;
          break;
        }
      }
    }

    $rows[] = array(
      $config->getTitle(),
      $config->getConnection()->getUrl(),
      $project_label,
      '<div class="semantic-connector-led" data-server-id="' . $config->getConnection()->getID() . '" data-server-type="pp-server" title="' . t('Checking service') . '"></div>',
      implode(' | ', $actions),
    );
  }

  $output['topic_pages'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('URL to PoolParty server'),
      t('Selected project'),
      t('Available'),
      t('Operations'),
    ),
    '#rows' => $rows,
    '#attributes' => array(
      'id' => 'topic-pages-configurations-table',
      'class' => array('semantic-connector-tablesorter'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'sonr_webmining_topic_pages') . '/js/sonr_webmining_topic_pages.admin.js'),
      'css' => array(drupal_get_path('module', 'sonr_webmining') . '/css/sonr_webmining.admin.css'),
    ),
  );

  // Add the tablesorter library if available.
  $tablesorter_path = 'sites/all/libraries/tablesorter';
  if (module_exists('libraries')) {
    $tablesorter_path = libraries_get_path('tablesorter');
  }
  if (file_exists($tablesorter_path . '/jquery.tablesorter.min.js')) {
    $output['#attached']['js'][] = libraries_get_path('tablesorter') . '/jquery.tablesorter.min.js';
  }

  return $output;
}

/**
 * A form to update the PP server connection of a PoolParty GraphSearch topic page.
 *
 * This form is used when creating a completely new topic page configuration
 * or when the PoolParty server connection needs to be changed or a different
 * project shell be used for an existing topic page configuration.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 * @param object $config
 *   A SonrWebminingTopicPage-instance
 *
 * @return array
 *   The Drupal form array.
 */
function sonr_webmining_topic_page_connection_form($form, &$form_state, $config = NULL) {
  if (!is_null($config) && !empty($config)) {
    $is_configuration_new = FALSE;
    $form['tpid'] = array(
      '#type' => 'hidden',
      '#value' => $config->getId(),
    );

    $form['title'] = array(
      '#type' => 'hidden',
      '#value' => $config->getTitle(),
    );
  }
  else {
    $is_configuration_new = TRUE;
    $config = new SonrWebminingTopicPage();

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Name of the topic page configuration.'),
      '#size' => 35,
      '#maxlength' => 255,
      '#default_value' => $config->getTitle(),
      '#required' => TRUE,
      '#validated' => TRUE,
    );
  }

  $form['server_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('PoolParty server connection settings'),
  );

  $connections = SemanticConnector::getConnectionsByType('pp_server');
  if (!empty($connections)) {
    $connection_options = array();
    foreach ($connections as $connection) {
      $connection_config = $connection->getConfig();
      if (!empty($connection_config['sonr_configuration'])) {
        $credentials = $connection->getCredentials();
        $connection_options[implode('|', array(
          $connection->getTitle(),
          $connection->getURL(),
          $credentials['username'],
          $credentials['password']
        ))] = $connection->getTitle();
      }
    }
    if (!empty($connection_options)) {
      $form['server_settings']['load_connection'] = array(
        '#type' => 'select',
        '#title' => t('Load an available PoolParty server'),
        '#options' => $connection_options,
        '#empty_option' => '',
        '#default_value' => '',
      );

      $form['server_settings']['load_connection_button'] = array(
        '#type' => 'button',
        '#value' => t('Load'),
        '#attributes' => array(
          'onclick' => '
        var connection_value = (jQuery(\'#edit-load-connection\').val());
        if (connection_value.length > 0) {
          var connection_details = connection_value.split(\'|\');
          jQuery(\'#edit-server-title\').val(connection_details[0]);
          jQuery(\'#edit-url\').val(connection_details[1]);
          jQuery(\'#edit-name\').val(connection_details[2]);
          jQuery(\'#edit-pass\').val(connection_details[3]);
        }
        return false;',
        ),
      );
    }
  }

  $connection = $config->getConnection();
  $form['server_settings']['connection_id'] = array(
    '#type' => 'hidden',
    '#value' => $connection->getID(),
  );

  $form['server_settings']['server_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Server title'),
    '#description' => t('A short title for the server below.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $connection->getTitle(),
    '#required' => TRUE,
  );

  $form['server_settings']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('URL, where the PoolParty server runs.'),
    '#size' => 35,
    '#maxlength' => 255,
    '#default_value' => $connection->getURL(),
    '#required' => TRUE,
  );

  $credentials = $connection->getCredentials();
  $form['server_settings']['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['server_settings']['credentials']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Name of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $credentials['username'],
  );
  $form['server_settings']['credentials']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 128,
    '#default_value' => $credentials['password'],
  );

  module_load_include('inc', 'sonr_webmining', 'includes/sonr_webmining.admin');
  $form['server_settings']['health_check'] = array(
    '#type' => 'button',
    '#value' => t('Health check'),
    '#ajax' => array(
      'callback' => 'sonr_webmining_new_available',
      'wrapper' => 'health_info',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );

  // Get the project options for the currently configured PoolParty server.
  $project_options = array();
  if (!$is_configuration_new) {
    $sonr_config = $connection->getApi('sonr')->getConfig();
    if (is_array($sonr_config)) {
      $projects = $connection->getApi('PPT')->getProjects();
      foreach ($projects as $project) {
        if ($project->id == $sonr_config['project']) {
          $project_options[$project->id] = $project->title . ' (' . $sonr_config['language'] . ')';
        }
      }
    }
  }
  // configuration set admin page.
  $form['project'] = array(
    '#type' => 'select',
    '#title' => 'Select a project',
    '#prefix' => '<div id="projects-replace">',
    '#suffix' => '</div>',
    '#options' => $project_options,
    '#default_value' => (!$is_configuration_new ? $config->getProjectId() : NULL),
    '#required' => TRUE,
    '#validated' => TRUE,
  );

  $form['load_projects'] = array(
    '#type' => 'button',
    '#value' => 'Load projects',
    '#ajax' => array(
      'event' => 'click',
      'callback' => 'sonr_webmining_connection_form_get_search_spaces',
      'wrapper' => 'projects-replace',
    ),
  );

  if ($is_configuration_new) {
    $markup = '<div id="health_info">' . t('Click to check if the server is available.') . '</div>';
  }
  else {
    $available = '<div id="health_info" class="available"><div class="semantic-connector-led led-green" title="Service available"></div>' . t('The server is available.') . '</div>';
    $not_available = '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . t('The server is not available or the credentials are incorrect.') . '</div>';
    $markup = $connection->available() ? $available : $not_available;
  }
  $form['server_settings']['health_info'] = array(
    '#markup' => $markup,
  );

  // Save and cancel buttons.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => ($is_configuration_new ? t('Save and edit details') : t('Save')),
    '#prefix' => '<div class="admin-form-submit-buttons" style="margin-top: 40px;">',
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/semantic-drupal/sonr-webmining',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * The validation handler for the PoolParty GraphSearch topic page connection form.
 */
function sonr_webmining_topic_page_connection_form_validate($form, &$form_state) {
  // Only do project validation during the save-operation, not during
  // ajax-requests like the health check of the server.
  if ($form_state['triggering_element']['#parents'][0] == 'save') {
    if (!isset($form_state['values']['tpid']) && empty($form_state['values']['title'])) {
      form_set_error('title', t('Name field is required.'));
    }

    // A project needs to be selected.
    if (empty($form_state['values']['project'])) {
      form_set_error('project', t('Please select a project.'));
    }
    // And it has to be a valid project (one that is connected to a PoolParty GraphSearch server).
    else {
      if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
        // Create a new connection (without saving) with the current form data.
        $connection = SemanticConnector::getConnection('pp_server');
        $connection->setURL($form_state['values']['url']);
        $connection->setCredentials(array(
          'username' => $form_state['values']['name'],
          'password' => $form_state['values']['pass'],
        ));
        $sonr_config = $connection->getApi('sonr')->getConfig();
        if (!is_array($sonr_config) || $sonr_config['project'] != $form_state['values']['project']) {
          form_set_error('project', t('There is no PoolParty GraphSearch server available for the selected project.'));
        }
      }
      else {
        form_set_error('url', t('The field URL must be a valid URL.'));
      }
    }
  }
}

/**
 * The submit handler for the PoolParty GraphSearch topic page connection form.
 */
function sonr_webmining_topic_page_connection_form_submit($form, &$form_state) {
  // Always create a new connection, if URL and type are the same the old one
  // will be used anyway.
  $connection = SemanticConnector::createConnection('pp_server', $form_state['values']['url'], $form_state['values']['server_title'], array(
    'username' => $form_state['values']['name'],
    'password' => $form_state['values']['pass'],
  ));

  // Update an existing configuration set.
  if (isset($form_state['values']['tpid'])) {
    $tpid = $form_state['values']['tpid'];
    db_update('sonr_webmining_topic_pages')
      ->fields(array(
        'connection_id' => $connection->getID(),
        'project_id' => $form_state['values']['project'],
        'entry_point' => serialize(array()),
      ))
      ->condition('tpid', $tpid)
      ->execute();

    drupal_set_message(t('The connection for PoolParty GraphSearch topic page %title has been updated.', array('%title' => $form_state['values']['title'])));
  }
  // Create a new configuration set.
  else {
    $tpid = db_insert('sonr_webmining_topic_pages')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'connection_id' => $connection->getID(),
        'project_id' => $form_state['values']['project'],
        'entry_point' => serialize(array()),
      ))
      ->execute();

    drupal_set_message(t('PoolParty GraphSearch topic page %title has been created.', array('%title' => $form_state['values']['title'])));
  }

  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $tpid;
}

/**
 * A form to edit an existing PoolParty GraphSearch topic page configuration.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 * @param object $config
 *   A SonrWebminingTopicPage-instance
 *
 * @return array
 *   The Drupal form array.
 */
function sonr_webmining_topic_page_form($form, &$form_state, $config) {
  $widget_types = sonr_webmining_topic_page_get_widget_types();

  $form['tpid'] = array(
    '#type' => 'hidden',
    '#value' => $config->getId(),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the topic page configuration.'),
    '#size' => 35,
    '#maxlength' => 255,
    '#default_value' => $config->getTitle(),
    '#required' => TRUE,
  );

  $connection = $config->getConnection();
  // Get the project title of the currently configured project.
  $project_title = '<invalid project selected>';
  $pp_server_projects = $connection->getApi('PPT')->getProjects();
  foreach ($pp_server_projects as $pp_server_project) {
    if ($pp_server_project->id == $config->getProjectId()) {
      $project_title = $pp_server_project->title;
    }
  }

  // Add information about the connection.
  $connection_markup = '';
  // Check the PoolParty server version if required.
  if (variable_get('semantic_connector_version_checking', TRUE)) {
    $version_messages = array();

    $ppx_api_version_info = $connection->getVersionInfo('PPX');
    if (version_compare($ppx_api_version_info['installed_version'], $ppx_api_version_info['latest_version'], '<')) {
      $version_messages[] = t('The connected PoolParty server is not up to date. You are currently running version %installedversion, upgrade to version %latestversion to enjoy the new features.', array('%installedversion' => $ppx_api_version_info['installed_version'], '%latestversion' => $ppx_api_version_info['latest_version']));
    }

    $sonr_api_version_info = $connection->getVersionInfo('sonr');
    if (version_compare($sonr_api_version_info['installed_version'], $sonr_api_version_info['latest_version'], '<')) {
      $version_messages[] = t('The connected PoolParty GraphSearch server is not up to date. You are currently running version %installedversion, upgrade to version %latestversion to enjoy the new features.', array('%installedversion' => $sonr_api_version_info['installed_version'], '%latestversion' => $sonr_api_version_info['latest_version']));
    }

    if (!empty($version_messages)) {
      $connection_markup .= '<div class="messages warning"><div class="message">' . implode('</div><div class="message">', $version_messages) . '</div></div>';
    }
  }
  $connection_markup .= '<p id="sonr-webmining-connection-info">Connected PoolParty server: <b>' . $connection->getTitle() . ' (' . $connection->getUrl() . ')</b><br />'
    . 'Selected project: <b>' . $project_title . '</b><br />'
    . l('Change the connected PoolParty server or project', 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $config->getId() . '/edit_connection') . '</p>';
  $form['pp_connection_markup'] = array(
    '#type' => 'markup',
    '#markup' => $connection_markup,
  );

  // Define the container for the vertical tabs.
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
  );

  // Tab: Result settings.
  $form['widgets'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widgets'),
    '#group' => 'settings',
  );

  $form['widgets']['widget_list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget List'),
    '#tree' => TRUE,
  );

  if (isset($form_state['triggering_element'])) {
    $max_widget_key = -1;
    $max_weight = -255;
    // Sort the widgets by their current weight.
    $widgets_by_weight = array();
    foreach ($form_state['values']['widget_list'] as $widget_key => $widget_values) {
      $widgets_by_weight[$widget_values['weight']][$widget_key] = $widget_values;

      if ($widget_values['weight'] > $max_weight) {
        $max_weight = $widget_values['weight'];
      }

      if ($widget_key > $max_widget_key) {
        $max_widget_key = $widget_key;
      }
    }
    ksort($widgets_by_weight);

    foreach ($widgets_by_weight as $weight => $widget_list) {
      foreach ($widget_list as $widget_key => $widget_values) {
        $widget_type = $widget_values['type'];
        $form['widgets']['widget_list'][$widget_key] = sonr_webmining_topic_page_form_build_widget(array_merge($widget_types[$widget_type], array('type' => $widget_type)), $weight);
      }
    }

    if ($form_state['triggering_element']['#parents'][0] == 'add_widget') {
      $new_widget_key = intval($max_widget_key) + 1;
      $selected_widget_type = $form_state['values']['add_widget_type'];
      $form['widgets']['widget_list'][$new_widget_key] = sonr_webmining_topic_page_form_build_widget(array_merge($widget_types[$selected_widget_type], array('type' => $selected_widget_type)), (intval($max_weight) + 1));
    }
  }
  else {
    // @todo: add the widget form data through the class methods.
    foreach ($config->getWidgets() as $widget) {

    }
  }

  $widget_type_options = array();
  foreach ($widget_types as $widget_type_id => $widget_type_info) {
    $widget_type_options[$widget_type_id] = $widget_type_info['name'];
  }
  $form['widgets']['add_widget_type'] = array(
    '#type' => 'select',
    '#title' => 'Type of widget to add',
    '#options' => $widget_type_options,
  );

  $form['widgets']['add_widget'] = array(
    '#type' => 'button',
    '#value' => t('Add new widget'),
    '#ajax' => array(
      'callback' => 'sonr_webmining_topic_pages_add_widget_ajax',
      'wrapper' => 'admin-widget-list-container',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );

  // Save and cancel buttons.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'button',
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'window.location.href = "' . url('admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $config->getId() . '/delete') . '"; return false;'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => (isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/semantic-drupal/sonr-webmining/topic-pages'),
  );

  // Add JavaScript
  //drupal_add_js(drupal_get_path('module', 'sonr_webmining') . '/js/sonr_webmining.admin.js');

  return $form;
}

/**
 * Theme function for the topic page configuration form.
 *
 * @param array $variables
 *   An array of variables supported by the theme.
 *
 * @return string
 *   The rendered HTML content of the admin form.
 */
function theme_sonr_webmining_topic_page_form($variables, $return_array = FALSE) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form['widgets']['widget_list']) as $widget_id) {
    $form['widgets']['widget_list'][$widget_id]['weight']['#attributes']['class'] = array('widget-list-order-weight');
    $rows[] = array(
      'data' => array(
        array('class' => array('slide-cross')),
        drupal_render($form['widgets']['widget_list'][$widget_id]['config']),
        drupal_render($form['widgets']['widget_list'][$widget_id]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array('Order', t('Widget'), t('Weight'));
  $form['widgets']['widget_list'] = array(
    '#type' => 'item',
    '#title' => t('Widgets'),
    '#description' => t('Drag and drop the handles of the widgets to set their order.'),
    '#markup' => theme(
      'table',
      array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array('id' => 'widget-list-order'),
      )
    ),
    '#prefix' => '<div id="admin-widget-list-container">',
    '#suffix' => '</div>',
  );

  drupal_add_tabledrag('widget-list-order', 'order', 'sibling', 'widget-list-order-weight');

  if ($return_array) {
    return $form;
  }

  $output = drupal_render_children($form);
  return $output;
}

function sonr_webmining_topic_page_form_build_widget($widget_info, $weight) {
  $widget_element = array();
  $class_file_path = DRUPAL_ROOT . '/' . $widget_info['file'];

  if (is_file($class_file_path)) {
    require_once ($class_file_path);
    $widget = new $widget_info['class']();

    $widget_element = array(
      '#type' => 'fieldset',
      '#title' => $widget_info['name'],
    );

    $widget_element['weight'] = array(
      '#type' => 'weight',
      // Weights from -255 to +255 are supported because of this delta.
      '#delta' => 255,
      '#title_display' => 'invisible',
      '#default_value' => $weight,
    );

    $widget_element['type'] = array(
      '#type' => 'value',
      '#value' => $widget->getType(),
    );
    $widget_element['config']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Administrative title of the widget'),
      '#size' => 35,
      '#maxlength' => 255,
    );

    $widget_element['config'] = array_merge($widget_element['config'], $widget->getAdminForm());
  }

  return $widget_element;
}

/**
 * Ajax callback function for adding a widget to the topic page config form.
 */
function sonr_webmining_topic_pages_add_widget_ajax($form, $form_state) {
  $themed_form = theme_sonr_webmining_topic_page_form(array('form' => $form), TRUE);
  return $themed_form['widgets']['widget_list'];
}

/**
 * Validate handler for the PoolParty GraphSearch topic page configuration form.
 */
function sonr_webmining_topic_page_form_validate($form, &$form_state) {
  $widget_types = sonr_webmining_topic_page_get_widget_types();

  if (isset($form_state['values']['widget_list'])) {
    foreach ($form_state['values']['widget_list'] as $widget_key => $widget_values) {
      $class_file_path = DRUPAL_ROOT . '/' . $widget_types[$widget_values['type']]['file'];

      if (is_file($class_file_path)) {
        require_once($class_file_path);
        $widget = new $widget_types[$widget_values['type']]['class']();
        $widget->validateAdminForm($form['widgets']['widget_list'][$widget_key]['config'], $widget_values['config']);
      }
    }
  }
}

/**
 * Submit handler for the PoolParty GraphSearch topic page configuration form.
 */
function sonr_webmining_topic_page_form_submit($form, &$form_state) {
  $widget_types = sonr_webmining_topic_page_get_widget_types();

  $widget_ids = array();
  if (isset($form_state['values']['widget_list'])) {
    foreach ($form_state['values']['widget_list'] as $widget_key => $widget_values) {
      $widget = new $widget_types[$widget_values['type']]['class']();
      $widget->submitAdminForm($form['widgets']['widget_list'][$widget_key]['config'], $widget_values['config']);

      // Widget already exists --> update it.
      if ($widget->getId() > 0) {
        db_update('sonr_webmining_topic_page_widgets')
          ->fields(array(
            'weight' => $widget_values['weight'],
            'title' => $widget_values['config']['title'],
            'filters' => serialize($widget->getFilters()),
            'display_config' => serialize($widget->getDisplayConfig()),
          ))
          ->condition('widget_id', $widget_values['widget_id'])
          ->execute();
        $widget_ids[] = $widget->getId();
      }
      // Widget does not yet exist --> create it.
      else {
        $new_widget_id = db_insert('sonr_webmining_topic_page_widgets')
          ->fields(array(
            'type' => $widget->getType(),
            'tpid' => $form_state['values']['tpid'],
            'weight' => $widget_values['weight'],
            'title' => $widget_values['config']['title'],
            'filters' => serialize($widget->getFilters()),
            'display_config' => serialize($widget->getDisplayConfig()),
          ))
          ->execute();
        $widget_ids[] = $new_widget_id;
      }
    }
  }

  // Delete all removed widgets out of the database.
  db_delete('sonr_webmining_topic_page_widgets')
    ->condition('tpid', $form_state['values']['tpid'])
    ->condition('widget_id', $widget_ids, 'NOT IN')
    ->execute();

  // Update the topic page itself.
  db_update('sonr_webmining_topic_pages')
    ->fields(array(
      'title' => $form_state['values']['title'],
    ))
    ->condition('tpid', $form_state['values']['tpid'])
    ->execute();

}

/**
 * The confirmation-form for cloning a topic page configuration.
 */
function sonr_webmining_topic_page_clone_form($form, &$form_state, $config) {
  $form_state['config'] = $config;
  return confirm_form($form,
    t('Are you sure you want to clone the PoolParty GraphSearch topic page configuration "%title"?', array('%title' => $config->getTitle())),
    'admin/config/semantic-drupal/sonr-webmining/topic-pages',
    '',
    t('Clone configuration'));
}

/**
 * The submit handler confirmation-form for cloning a topic page configuration.
 */
function sonr_webmining_topic_page_clone_form_submit($form, &$form_state) {
  $config = $form_state['config'];

  $tpid = db_insert('sonr_webmining_topic_pages')
    ->fields(array(
      'title' => $config->getTitle() . ' (CLONE)',
      'connection_id' => $config->getConnectionId(),
      'project_id' => $config->getProjectId(),
      'entry_point' => serialize($config->getEntryPoint()),
    ))
    ->execute();

  // @todo: also clone all the widgets of this topic page.

  drupal_set_message(t('%title has been cloned.', array('%title' => $config->getTitle())));
  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/topic-pages/' . $tpid;
}

/**
 * The confirmation-form for deleting a topic page configuration.
 */
function sonr_webmining_topic_page_delete_form($form, &$form_state, $config) {
  $form_state['config'] = $config;
  return confirm_form($form,
    t('Are you sure you want to delete PoolParty GraphSearch topic page configuration "%title"?', array('%title' => $config->getTitle())),
    'admin/config/semantic-drupal/sonr-webmining/topic-pages',
    t('This action cannot be undone.'),
    t('Delete configuration'));
}

/**
 * The submit handler confirmation-form for deleting a topic page configuration.
 */
function sonr_webmining_topic_page_delete_form_submit($form, &$form_state) {
  $config = $form_state['config'];

  db_delete('sonr_webmining_topic_pages')
    ->condition('tpid', $config->getId())
    ->execute();

  drupal_set_message(t('%title has been deleted.', array('%title' => $config->getTitle())));
  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/topic-pages';
}
