<?php

/**
 * @file
 * The class for PoolParty GraphSearch topic page configuration.
 */

class SonrWebminingTopicPage {

  protected $id;
  protected $project_id;
  protected $title;
  protected $connectionId;
  protected $connection;
  protected $entry_point;
  protected $widgets;

  /**
   * Constructor of the SonrWebminingTopicPage-class.
   *
   * @param int $config_id
   *   ID of the topic page configuration
   */
  public function __construct($config_id = 0) {
    // Load configuration set.
    $config_set = db_select('sonr_webmining_topic_pages', 'swts')
      ->fields('swts')
      ->condition('tpid', $config_id)
      ->execute()
      ->fetch();

    if ($config_set) {
      $this->id = $config_id;
      $this->project_id = $config_set->project_id;
      $this->title = $config_set->title;
      $this->connectionId = $config_set->connection_id;
      $this->connection = SemanticConnector::getConnection('pp_server', $config_set->connection_id);
      $this->entry_point = unserialize($config_set->entry_point);
      // @todo: add widgets correctly.
      $this->widgets = array();
    }
    else {
      $this->id = 0;
      $this->project_id = '';
      $this->title = '';
      $this->connectionId = 0;
      $this->connection = SemanticConnector::getConnection('pp_server', $this->connectionId);
      $this->entry_point = array();
      $this->widgets = array();
    }
  }

  /**
   * Getter-function for the ID of the topic page.
   *
   * @return int
   *   ID of the topic page configuration
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter-function for the project_id-variable.
   *
   * @return string
   *   ID of the used PoolParty project.
   */
  public function getProjectId() {
    return $this->project_id;
  }

  /**
   * Getter-function for the title-variable.
   *
   * @return string
   *   Title of the topic page configuration
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server ID.
   *
   * @return int
   *   PoolParty GraphSearch server ID of the topic page configuration
   */
  public function getConnectionId() {
    return $this->connectionId;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server connection object.
   *
   * @return SemanticConnectorConnection
   *   PoolParty GraphSearch server connection object of the topic page configuration
   */
  public function getConnection() {
    return $this->connection;
  }

  /**
   * Getter-function for the entry_point-variable.
   *
   * @return array
   *   The entry point of this topic page
   */
  public function getEntryPoint() {
    return $this->entry_point;
  }

  /**
   * Getter-function for the widgets-variable.
   *
   * @return array
   *   The widgets connected to this topic page
   */
  public function getWidgets() {
    return $this->widgets;
  }
}
