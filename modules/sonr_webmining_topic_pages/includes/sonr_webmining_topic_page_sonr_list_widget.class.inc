<?php

/**
 * @file
 * The class for a PoolParty GraphSearch topic page PoolParty GraphSearch Content List widget.
 */

class SonrWebminingTopicPageSonrListWidget extends SonrWebminingTopicPageWidget {
  public function __construct($config_id = 0) {
    parent::__construct();
    $this->type = 'sonr_list';
  }

  public function getAdminForm() {
    $widget_element = array();
    $widget_element['block_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Block-title of the widget'),
      '#size' => 35,
      '#maxlength' => 255,
    );

    return $widget_element;
  }

  public function validateAdminForm($structure, $values) { form_set_error(implode('][', $structure['block_title']['#parents']), t('Block title is bad!')); }

  public function submitAdminForm($structure, $values) {
    $this->display_config = $values;
  }
}
