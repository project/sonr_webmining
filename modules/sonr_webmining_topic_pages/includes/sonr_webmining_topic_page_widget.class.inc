<?php

/**
 * @file
 * The abstract class for a PoolParty GraphSearch topic page widget.
 */

abstract class SonrWebminingTopicPageWidget {
  protected $id;
  protected $type;
  protected $tpid;
  protected $title;
  protected $filters;
  protected $display_config;

  /**
   * Constructor of the SonrWebminingTopicPage-class.
   *
   * @param int $config_id
   *   ID of the topic page configuration
   */
  public function __construct($config_id = 0) {
    // Load configuration set.
    $config_set = db_select('sonr_webmining_topic_page_widgets', 'tpw')
      ->fields('tpw')
      ->condition('widget_id', $config_id)
      ->execute()
      ->fetch();

    if ($config_set) {
      $this->id = $config_id;
      $this->type = $config_set->type;
      $this->tpid = $config_set->tpid;
      $this->title = $config_set->title;
      $this->filters = unserialize($config_set->filters);
      $this->display_config = unserialize($config_set->display_config);
    }
    else {
      $this->id = 0;
      $this->type = '';
      $this->tpid = 0;
      $this->title = '';
      $this->filters = array();
      $this->display_config = array();
    }
  }

  /**
   * Getter-function for the ID of the topic page widget.
   *
   * @return int
   *   ID of the topic page widget
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter-function for the type of the topic page widget.
   *
   * @return string
   *   Type of the topic page widget
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Getter-function for the topic page ID of the topic page widget.
   *
   * @return int
   *   Topic page ID of the topic page widget
   */
  public function getTpid() {
    return $this->tpid;
  }

  /**
   * Getter-function for the title of the topic page widget.
   *
   * @return string
   *   Title of the topic page widget
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Getter-function for the filters of the topic page widget.
   *
   * @return array
   *   Filters of the topic page widget
   */
  public function getFilters() {
    return $this->filters;
  }

  /**
   * Getter-function for the display configuration of the topic page widget.
   *
   * @return array
   *   Display configuration of the topic page widget
   */
  public function getDisplayConfig() {
    return $this->display_config;
  }

  public abstract function getAdminForm();
  public abstract function validateAdminForm($structure, $values);
  public abstract function submitAdminForm($structure, $values);
}
