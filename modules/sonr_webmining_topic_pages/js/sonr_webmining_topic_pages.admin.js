(function ($) {
  Drupal.behaviors.sonr_webmining_topic_pages  = {
    attach: function () {
      // Make the configuration tables sortable if tablesorter is available.
      if ($.isFunction($.fn.tablesorter)) {
        $("table#topic-pages-configurations-table").tablesorter({
          widgets: ["zebra"],
          widgetOptions: {
            zebra: ["odd", "even"]
          },
          sortList: [[0, 0]],
          headers: {
            2: { sorter: false },
            3: { sorter: false }
          }
        });
      }
    }
  };

})(jQuery);
