<?php
/**
 * @file
 * Administration functionality for module PoolParty GraphSearch SeeAlso Engine.
 */

/**
 * List all saved PoolParty GraphSearch Similar configurations.
 *
 * @return array
 *   The renderable HTML of the list of configurations.
 */
function sonr_webmining_similar_configuration_list() {
  $output = array();

  $output['sonr_webmining_title'] = array(
    '#markup' => '<h3 class="semantic-connector-table-title">' . t('PoolParty GraphSearch SeeAlso widgets') . '</h3>',
  );

  $rows = array();
  $config_sets = sonr_webmining_similar_config_load_multiple();
  /** @var \SonrWebminingSimilarConfig $config */
  foreach ($config_sets as $config) {
    $actions = array(
      l(t('Edit'), 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $config->getId()),
      l(t('Go to block'), 'admin/structure/block/manage/sonr_webmining_similar/sonr_webmining_similar_widget_' . $config->getId() . '/configure'),
      l(t('Delete'), 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $config->getId() . '/delete'),
    );

    // Get the search space label.
    $search_space_id = $config->getSearchSpaceId();
    /** @var \SemanticConnectorPPServerConnection $connection */
    $connection =  $config->getConnection();
    $connection_config = $connection->getConfig();
    $sonr_config = $connection_config['sonr_configuration'];
    $search_space_label = 'search space not found';
    if (is_array($sonr_config)) {
      if (version_compare($sonr_config['version'], '6.1', '>=')) {
        $search_spaces = semantic_connector_get_graphsearch_search_spaces($sonr_config);
        foreach ($search_spaces as $search_space) {
          if ($search_space['id'] == $search_space_id) {
            $search_space_label = $search_space['name'];
            break;
          }
        }
      }
      else {
        $projects = $connection->getApi('PPT')->getProjects();
        foreach ($projects as $project) {
          if (isset($sonr_config['projects'][$project->id]) && $project->id == $search_space_id) {
            $search_space_label = $project->title;
            break;
          }
        }
      }
    }

    $title = '<div class="semantic-connector-led" data-server-id="' . $connection->getId() . '" data-server-type="pp-server" title="' . t('Checking service') . '"></div>';
    $title .= $config->getTitle();
    $rows[] = array(
      $title,
      l($connection->getTitle(), $connection->getUrl() . '/' . $connection->getGraphSearchPath(), array('attributes' => array('target' => '_blank'))),
      $search_space_label,
      implode(' | ', $actions),
    );
  }

  $output['sonr_webmining'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('PoolParty GraphSearch server'),
      t('Selected search space'),
      t('Operations'),
    ),
    '#rows' => $rows,
    '#attributes' => array(
      'id' => 'sonr-webmining-similar-configurations-table',
      'class' => array('semantic-connector-tablesorter'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'sonr_webmining_similar') . '/js/sonr_webmining_similar.admin.js'),
    ),
  );

  // Add the tablesorter library if available.
  $tablesorter_path = 'sites/all/libraries/tablesorter';
  if (module_exists('libraries')) {
    $tablesorter_path = libraries_get_path('tablesorter');
  }
  if (file_exists($tablesorter_path . '/jquery.tablesorter.min.js')) {
    $output['#attached']['js'][] = libraries_get_path('tablesorter') . '/jquery.tablesorter.min.js';
  }

  return $output;
}

/**
 * Ajax callback function for checking if a new PoolParty GraphSearch server
 * is available.
 */
function sonr_webmining_similar_new_available($form, $form_state) {
  $available = '<div id="health_info" class="available"><div class="semantic-connector-led led-green" title="Service available"></div>' . t('The server is available.') . '</div>';
  $not_available = '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . t('The server is not available or the credentials are incorrect.') . '</div>';

  if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
    // Search for already existing connections.
    // (added to make overrides work here).
    $potential_connections = SemanticConnector::searchConnections(array(
      'type' => 'pp_server',
      'url' => $form_state['values']['url'],
    ));

    // Connection already exists, use the first one found.
    if (!empty($potential_connections)) {
      $connection = reset($potential_connections);
    }
    else {
      $connection = SemanticConnector::getConnection('pp_server');
      $connection->setUrl($form_state['values']['url']);
    }
    $connection->setCredentials(array(
      'username' => $form_state['values']['name'],
      'password' => $form_state['values']['pass'],
    ));

    $availability = $connection->getApi('PPX')->available();
    if (isset($availability['message']) && !empty($availability['message'])) {
      return '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . $availability['message'] . '</div>';
    }
    else {
      return $availability['success'] ? $available : $not_available;
    }
  }

  return $not_available;
}

/**
 * A form to update the PP server connection of a SeeAlso config.
 *
 * This form is used when creating a completely new SeeAlso
 * widget or when the PoolParty server connection needs to be changed
 * or a different project shall be used for an existing SeeAlso widget.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 * @param object $config
 *   A SonrWebminingSimilarConfig-instance
 *
 * @return array
 *   The Drupal form array.
 */
function sonr_webmining_similar_connection_form($form, &$form_state, $config = NULL) {
  if (!is_null($config) && !empty($config)) {
    $is_configuration_new = FALSE;
    $form['id'] = array(
      '#type' => 'hidden',
      '#value' => $config->getId(),
    );

    $form['title'] = array(
      '#type' => 'hidden',
      '#value' => $config->getTitle(),
    );
  }
  else {
    $is_configuration_new = TRUE;
    $config = new SonrWebminingSimilarConfig();

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Name of the SeeAlso widget.'),
      '#size' => 35,
      '#maxlength' => 255,
      '#default_value' => $config->getTitle(),
      '#required' => TRUE,
      '#validated' => TRUE,
    );
  }

  // Tab: Server settings.
  $form['server_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('1. Select the PoolParty server to use'),
  );

  $connections = SemanticConnector::getConnectionsByType('pp_server');
  if (!empty($connections)) {
    $connection_options = array();
    /** @var \SemanticConnectorPPServerConnection $connection */
    foreach ($connections as $connection) {
      $connection_config = $connection->getConfig();
      if (!empty($connection_config['sonr_configuration'])) {
        $credentials = $connection->getCredentials();
        $connection_options[implode('|', array(
          $connection->getTitle(),
          $connection->getUrl(),
          $credentials['username'],
          $credentials['password']
        ))] = $connection->getTitle();
      }
    }
    if (!empty($connection_options)) {
      $form['server_settings']['load_connection'] = array(
        '#type' => 'select',
        '#title' => t('Load an available PoolParty server'),
        '#options' => $connection_options,
        '#empty_option' => '',
        '#default_value' => '',
      );
    }
  }

  $connection = $config->getConnection();
  $form['server_settings']['connection_id'] = array(
    '#type' => 'hidden',
    '#value' => $connection->getId(),
  );

  $form['server_settings']['server_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Server title'),
    '#description' => t('A short title for the server below.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $connection->getTitle(),
    '#required' => TRUE,
  );

  $form['server_settings']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('URL, where the PoolParty server runs.'),
    '#size' => 35,
    '#maxlength' => 255,
    '#default_value' => $connection->getUrl(),
    '#required' => TRUE,
  );

  $credentials = $connection->getCredentials();
  $form['server_settings']['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['server_settings']['credentials']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Name of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $credentials['username'],
  );
  $form['server_settings']['credentials']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 128,
    '#default_value' => $credentials['password'],
  );

  $form['server_settings']['health_check'] = array(
    '#type' => 'button',
    '#value' => t('Health check'),
    '#ajax' => array(
      'callback' => 'sonr_webmining_similar_new_available',
      'wrapper' => 'health_info',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );

  if ($is_configuration_new) {
    $markup = '<div id="health_info">' . t('Click to check if the server is available.') . '</div>';
  }
  else {
    $available = '<div id="health_info" class="available"><div class="semantic-connector-led led-green" title="Service available"></div>' . t('The server is available.') . '</div>';
    $not_available = '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . t('The server is not available or the credentials are incorrect.') . '</div>';
    $markup = $connection->available() ? $available : $not_available;
  }
  $form['server_settings']['health_info'] = array(
    '#markup' => $markup,
  );

  // Container: Search space loading.
  $form['search_space_load'] = array(
    '#type' => 'fieldset',
    '#title' => t('2. Load the search spaces'),
  );

  $form['search_space_load']['load_search_spaces'] = array(
    '#type' => 'button',
    '#value' => 'Load search spaces',
    '#ajax' => array(
      'event' => 'click',
      'callback' => 'sonr_webmining_similar_connection_form_get_search_spaces',
      'wrapper' => 'search_spaces-replace',
    ),
  );

  // Container: Search Space selection.
  $form['search_space_select'] = array(
    '#type' => 'fieldset',
    '#title' => t('3. Select the search space to use'),
    '#description' => t('Note: In case this list is still empty after clicking the "Load search spaces" button make sure that a connection to the PoolParty server can be established and check the rights of your selected user inside PoolParty.'),
  );

  // Get the search space options for the currently configured PoolParty server.
  $search_space_options = array();
  if (!$is_configuration_new) {
    $connection_config = $connection->getConfig();
    $sonr_config = $connection_config['sonr_configuration'];
    if (is_array($sonr_config)) {
      if (is_array($sonr_config)) {
        if (version_compare($sonr_config['version'], '6.1', '>=')) {
          $search_spaces = semantic_connector_get_graphsearch_search_spaces($sonr_config);
          foreach ($search_spaces as $search_space) {
            $search_space_options[$search_space['id']] = $search_space['name'] . ' (' . $search_space['language'] . ')';
          }
        }
        else {
          $projects = $connection->getApi('PPT')->getProjects();
          foreach ($projects as $project) {
            if (is_array($sonr_config) && isset($sonr_config['projects'][$project->id])) {
              $search_space_options[$project->id] = $project->title . ' (' . $sonr_config['projects'][$project->id]['search_spaces'][$project->id]['language'] . ')';
            }
          }
        }
      }
    }
  }
  // configuration set admin page.
  $form['search_space_select']['search_space'] = array(
    '#type' => 'select',
    '#title' => 'Select a search space',
    '#prefix' => '<div id="search_spaces-replace">',
    '#suffix' => '</div>',
    '#options' => $search_space_options,
    '#default_value' => (!$is_configuration_new ? $config->getSearchSpaceId() : NULL),
    '#required' => TRUE,
    '#validated' => TRUE,
  );

  // Save and cancel buttons.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => ($is_configuration_new ? t('Save and edit details') : t('Save')),
    '#prefix' => '<div class="admin-form-submit-buttons" style="margin-top: 40px;">',
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar',
    '#suffix' => '</div>',
  );

  // Add JS files
  $form['#attached'] = array(
    'js' => array(drupal_get_path('module', 'sonr_webmining_similar') . '/js/sonr_webmining_similar.admin.js'),
  );

  return $form;
}

/**
 * Ajax callback function to get a search space select list for a given
 * PoolParty server connection configuration.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 *
 * @return array
 *   The select form element containing the search space options for the current
 *   PoolParty server connection.
 */
function sonr_webmining_similar_connection_form_get_search_spaces($form, $form_state) {
  $search_space_element = $form['search_space_select']['search_space'];

  $search_space_options = array();
  if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
    // Search for already existing connections.
    // (added to make overrides work here).
    $potential_connections = SemanticConnector::searchConnections(array(
      'type' => 'pp_server',
      'url' => $form_state['values']['url'],
    ));

    // Connection already exists, use the first one found.
    if (!empty($potential_connections)) {
      $connection = reset($potential_connections);
    }
    else {
      $connection = SemanticConnector::getConnection('pp_server');
      $connection->setUrl($form_state['values']['url']);
    }

    $connection->setCredentials(array(
      'username' => $form_state['values']['name'],
      'password' => $form_state['values']['pass'],
    ));
    $sonr_config = $connection->getApi('sonr')->getConfig();
    $connection_config = $connection->getConfig();
    if (is_array($sonr_config)) {
      if (version_compare($connection_config['sonr_configuration']['version'], '6.1', '>=')) {
        $search_spaces = semantic_connector_get_graphsearch_search_spaces($sonr_config);
        foreach ($search_spaces as $search_space) {
          $search_space_options[$search_space['id']] = $search_space['name'] . ' (' . $search_space['language'] . ')';
        }
      }
      else {
        $projects = $connection->getApi('PPT')->getProjects();
        foreach ($projects as $project) {
          if (is_array($sonr_config) && isset($sonr_config['projects'][$project->id])) {
            $search_space_options[$project->id] = $project->title . ' (' . $sonr_config['projects'][$project->id]['search_spaces'][$project->id]['language'] . ')';
          }
        }
      }
    }
  }

  $search_space_element['#options'] = $search_space_options;
  return $search_space_element;
}

/**
 * The validation handler for the SeeAlso connection form.
 */
function sonr_webmining_similar_connection_form_validate($form, &$form_state) {
  // Only do search space validation during the save-operation, not during
  // ajax-requests like the health check of the server.
  if ($form_state['triggering_element']['#parents'][0] == 'save') {
    // A title is required.
    if (!isset($form_state['values']['id']) && empty($form_state['values']['title'])) {
      form_set_error('title', t('Name field is required.'));
    }

    // A search space needs to be selected.
    if (empty($form_state['values']['search_space'])) {
      form_set_error('search_space', t('Please select a search space.'));
    }
    // And it has to be a valid search space (one that is connected to a
    // PoolParty GraphSearch server).
    else {
      if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
        // Search for already existing connections.
        // (added to make overrides work here).
        $potential_connections = SemanticConnector::searchConnections(array(
          'type' => 'pp_server',
          'url' => $form_state['values']['url'],
        ));

        // Connection already exists, use the first one found.
        if (!empty($potential_connections)) {
          $connection = reset($potential_connections);
        }
        else {
          $connection = SemanticConnector::getConnection('pp_server');
          $connection->setUrl($form_state['values']['url']);
        }
        $connection->setCredentials(array(
          'username' => $form_state['values']['name'],
          'password' => $form_state['values']['pass'],
        ));
        $sonr_config = $connection->getApi('sonr')->getConfig();
        $connection_config = $connection->getConfig();
        if (version_compare($connection_config['sonr_configuration']['version'], '6.1', '>=')) {
          $search_spaces = semantic_connector_get_graphsearch_search_spaces($sonr_config);
          if (!isset($search_spaces[$form_state['values']['search_space']])) {
            form_set_error('search_space', t('There is no PoolParty GraphSearch server available for the selected search space.'));
          }
        }
        else {
          if (!is_array($sonr_config) || !isset($sonr_config['projects'][$form_state['values']['search_space']])) {
            form_set_error('search_space', t('There is no PoolParty GraphSearch server available for the selected search space.'));
          }
        }
      }
      else {
        form_set_error('url', t('The field URL must be a valid URL.'));
      }
    }
  }
}

/**
 * The submit handler for the SeeAlso connection form.
 */
function sonr_webmining_similar_connection_form_submit($form, &$form_state) {
  // Always create a new connection, if URL and type are the same the old one
  // will be used anyway.
  $connection = SemanticConnector::createConnection('pp_server', $form_state['values']['url'], $form_state['values']['server_title'], array(
    'username' => $form_state['values']['name'],
    'password' => $form_state['values']['pass'],
  ));

  // Update an existing configuration set.
  if (isset($form_state['values']['id'])) {
    $id = $form_state['values']['id'];
    db_update('sonr_webmining_similar_widgets')
      ->fields(array(
        'connection_id' => $connection->getId(),
        'search_space_id' => $form_state['values']['search_space'],
      ))
      ->condition('id', $id)
      ->execute();

    drupal_set_message(t('The connection for PoolParty GraphSearch SeeAlso widget %title has been updated.', array('%title' => $form_state['values']['title'])));
  }
  // Create a new configuration set.
  else {
    $id = db_insert('sonr_webmining_similar_widgets')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'connection_id' => $connection->getId(),
        'search_space_id' => $form_state['values']['search_space'],
        'config' => serialize(SonrWebminingSimilarConfig::getDefaultConfig())
      ))
      ->execute();

    drupal_set_message(t('The connection for PoolParty GraphSearch SeeAlso widget %title has been created.', array('%title' => $form_state['values']['title'])));
  }

  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $id;
}

/**
<<<<<<< HEAD
 * Add a SeeAlso widget for a specified PoolParty GraphSearch
 * configuration and project.
=======
 * Add a Similar Content configuration for a specified PoolParty GraphSearch
 * configuration and search space.
>>>>>>> 7.x-2.x-PPGS6.1
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 * @param int $connection_id
 *   The ID of the PoolParty server connection.
 * @param string $project_id
 *   The ID of the PoolParty project to use.
 *
 * @return array
 *   The Drupal form array of the confirmation form.
 */
function sonr_webmining_similar_configuration_from_sonr_config_form($form, &$form_state, $connection_id, $project_id) {
  $connection = SemanticConnector::getConnection('pp_server', $connection_id);
  $pp_config = $connection->getConfig();
  if (!empty($pp_config)) {
    foreach ($pp_config['projects'] as $project) {
      if ($project->id == $project_id) {
        $form['connection_id'] = array(
          '#type' => 'value',
          '#value' => $connection_id,
        );
        $form['project_id'] = array(
          '#type' => 'value',
          '#value' => $project_id,
        );
        $form['description'] = array(
          '#markup' => t('Selected PoolParty server:') . ' <b>' . $connection->getTitle() . '</b><br />' . t('Selected project:') . ' <b>' . $project->title . '</b>',
        );

        if (!empty($pp_config['sonr_configuration']) && isset($pp_config['sonr_configuration']['projects'][$project->id])) {
          if (version_compare($pp_config['sonr_configuration']['version'], '6.1', '>=')) {
            $search_spaces = array();
            foreach ($pp_config['sonr_configuration']['projects'][$project->id]['search_spaces'] as $search_space) {
              $search_spaces[$search_space['id']] = $search_space['name'];
            }
            $form['search_space_id'] = array(
              '#type' => 'select',
              '#title' => t('Select search space'),
              '#options' => $search_spaces,
            );
          }
          else {
            $form['search_space_id'] = array(
              '#type' => 'value',
              '#value' => $project_id,
            );
          }
        }

        // Save and cancel buttons.
        $form['save'] = array(
          '#type' => 'submit',
          '#value' => t('Create configuration'),
          '#prefix' => '<div class="form-actions form-wrapper">',
        );
        $form['cancel'] = array(
          '#type' => 'link',
          '#title' => t('Cancel'),
          '#href' => (isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/'),
          '#suffix' => '</div>',
        );

        return $form;
      }
    }
  }

  $form['description'] = array(
    '#markup' => '<p>' . t('Invalid Semantic Connector connection or project selected.') . '</p>',
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => (isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/semantic-drupal/sonr-webmining'),
  );

  return $form;
}

/**
 * Validation handler for sonr_webmining_similar_configuration_from_sonr_config_form().
 */
function sonr_webmining_similar_configuration_from_sonr_config_form_validate($form, &$form_state) {
  if (!isset($form_state['values']['search_space_id']) || empty($form_state['values']['search_space_id'])) {
    form_set_error('search_space_id', t('Please select a search space.'));
  }
}

/**
 * Submit handler for sonr_webmining_similar_configuration_from_sonr_config_form().
 */
function sonr_webmining_similar_configuration_from_sonr_config_form_submit($form, &$form_state) {
  $connection = SemanticConnector::getConnection('pp_server', $form_state['values']['connection_id']);
  $pp_config = $connection->getConfig();
  $project_id = $form_state['values']['project_id'];
  $search_space_id = $form_state['values']['search_space_id'];

  // The project has a configured PoolParty GraphSearch server.
  if (isset($pp_config['sonr_configuration']) && !empty($pp_config['sonr_configuration']) && isset($pp_config['sonr_configuration']['projects'][$project_id]) && isset($pp_config['sonr_configuration']['projects'][$project_id]['search_spaces']) && isset($pp_config['sonr_configuration']['projects'][$project_id]['search_spaces'][$search_space_id])) {
    foreach ($pp_config['projects'] as $project) {
      if ($project->id == $project_id) {
        // Create a new widget.
        $title = 'PoolParty GraphSearch SeeAlso widget for ' . $connection->getTitle() . ' (' . $project->title . ')';
        $id = db_insert('sonr_webmining_similar_widgets')
          ->fields(array(
            'title' => $title,
            'connection_id' => $connection->getId(),
            'search_space_id' => $search_space_id,
            'config' => serialize(SonrWebminingSimilarConfig::getDefaultConfig()),
          ))
          ->execute();

        drupal_set_message(t('PoolParty GraphSearch SeeAlso widget "%title" has been created.', array('%title' => $title)));
        // Drupal Goto to forward a destination if one is available.
        if (isset($_GET['destination'])) {
          $destination = $_GET['destination'];
          unset($_GET['destination']);
          drupal_goto('admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $id, array('query' => array('destination' => $destination)));
          exit();
        }
        $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $id;
        break;
      }
    }
  }
  // No PoolParty GraphSearch server available for the selected project.
  else {
    drupal_set_message(t('There is no search space with the ID "%searchspaceid" for the PoolParty GraphSearch configuration on PoolParty server "%ppservertitle"', array('%searchspaceid' => $search_space_id,'%ppservertitle' => $connection->getTitle())), 'error');
    $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining';
  }
}

/**
 * A form to edit an existing PoolParty GraphSearch SeeAlso widget.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form_state array.
 * @param object $config
 *   A SonrWebminingConfigurationSet-instance
 *
 * @return array
 *   The Drupal form array.
 */
function sonr_webmining_similar_configuration_form($form, &$form_state, SonrWebminingSimilarConfig $config) {
  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $config->getId(),
  );
  $configuration = $config->getConfig();

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of the PoolParty GraphSearch SeeAlso widget.'),
    '#size' => 35,
    '#maxlength' => 255,
    '#default_value' => $config->getTitle(),
    '#required' => TRUE,
  );

  $connection = $config->getConnection();
  // Get the search space label.
  $search_space_id = $config->getSearchSpaceId();
  $connection_config = $connection->getConfig();
  $sonr_config = $connection_config['sonr_configuration'];
  $search_space_label = t('<invalid search space selected>');
  if (is_array($sonr_config)) {
    if (is_array($sonr_config)) {
      if (version_compare($sonr_config['version'], '6.1', '>=')) {
        $search_spaces = semantic_connector_get_graphsearch_search_spaces($sonr_config);
        foreach ($search_spaces as $search_space) {
          if ($search_space['id'] == $search_space_id) {
            $search_space_label = $search_space['name'];
            break;
          }
        }
      }
      else {
        $projects = $connection->getApi('PPT')->getProjects();
        foreach ($projects as $project) {
          if (isset($sonr_config['projects'][$project->id]) && $project->id == $search_space_id) {
            $search_space_label = $project->title;
            break;
          }
        }
      }
    }
  }

  // Add information about the connection.
  $connection_markup = '';
  // Check the PoolParty server version if required.
  if (variable_get('semantic_connector_version_checking', TRUE)) {
    $version_messages = array();

    $sonr_api_version_info = $connection->getVersionInfo('sonr');
    if (version_compare($sonr_api_version_info['installed_version'], $sonr_api_version_info['latest_version'], '<')) {
      $version_messages[] = t('The connected PoolParty GraphSearch server is not up to date. You are currently running version %installedversion, upgrade to version %latestversion to enjoy the new features.', array('%installedversion' => $sonr_api_version_info['installed_version'], '%latestversion' => $sonr_api_version_info['latest_version']));
    }

    if (!empty($version_messages)) {
      $connection_markup .= '<div class="messages warning"><div class="message">' . implode('</div><div class="message">', $version_messages) . '</div></div>';
    }
  }
  $connection_markup .= '<p id="sonr-webmining-connection-info">' . t('Connected PoolParty server') . ': <b>' . $connection->getTitle() . ' (' . $connection->getUrl() . ')</b><br />'
    . t('Selected search space') . ': <b>' . $search_space_label . '</b><br />'
    . l(t('Change the connected PoolParty server or project'), 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $config->getId() . '/edit_connection') . '</p>';
  $form['pp_connection_markup'] = array(
    '#type' => 'markup',
    '#markup' => $connection_markup,
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget settings'),
  );
  $form['settings']['max_items'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items to display'),
    '#description' => t('The maximum number of similar documents you want to display.'),
    '#size' => 15,
    '#maxlength' => 5,
    '#default_value' => $configuration['max_items'],
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  // Save and cancel buttons.
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['delete'] = array(
    '#type' => 'button',
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'window.location.href = "' . url('admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar/' . $config->getId() . '/delete') . '"; return false;'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => (isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar'),
  );
  $form['#attached'] = array(
    'js' => array(drupal_get_path('module', 'sonr_webmining_similar') . '/js/sonr_webmining_similar.admin.js'),
  );

  return $form;
}

/**
 * The submit handler for the PoolParty GraphSearch SeeAlso widget form.
 */
function sonr_webmining_similar_configuration_form_submit($form, &$form_state) {
  $previous_configuration_set = new SonrWebminingSimilarConfig($form_state['values']['id']);

  $config = array(
    'max_items' => $form_state['values']['max_items'],
  );

  // Update an existing configuration set.
  if (isset($form_state['values']['id'])) {
    db_update('sonr_webmining_similar_widgets')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'config' => serialize($config),
      ))
      ->condition('id', $form_state['values']['id'])
      ->execute();
  }

  drupal_set_message(t('PoolParty GraphSearch SeeAlso widget %title has been saved.', array('%title' => $previous_configuration_set->getTitle())));
  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar';
}

/**
 * The confirmation form for deleting a SeeAlso widget.
 */
function sonr_webmining_similar_configuration_delete_form($form, &$form_state, $config) {
  $form_state['config'] = $config;
  return confirm_form($form,
    t('Are you sure you want to delete the PoolParty GraphSearch SeeAlso widget "%title"?', array('%title' => $config->getTitle())),
    'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar',
    t('This action cannot be undone.'),
    t('Delete configuration'));
}

/**
 * The submit handler for deleting a SeeAlso widget.
 */
function sonr_webmining_similar_configuration_delete_form_submit($form, &$form_state) {
  $config = $form_state['config'];

  db_delete('sonr_webmining_similar_widgets')
    ->condition('id', $config->getId())
    ->execute();

  db_delete('block')
    ->condition('module', 'sonr_webmining_similar')
    ->condition('delta', 'sonr_webmining_similar_widget_' . $config->getId())
    ->execute();

  drupal_set_message(t('%title has been deleted.', array('%title' => $config->getTitle())));
  $form_state['redirect'] = 'admin/config/semantic-drupal/sonr-webmining/sonr-webmining-similar';
}
