<?php
/**
 * @file
 * The class for PoolParty GraphSearch SeeAlso widget.
 */

class SonrWebminingSimilarConfig {

  protected $id;
  protected $title;
  protected $connection_id;
  protected $search_space_id;
  /** @var \SemanticConnectorPPServerConnection  */
  protected $connection;
  protected $config;

  /**
   * Constructor of the SonrWebminingConfigurationSet-class.
   *
   * @param int $config_id
   *   ID of the configuration set
   */
  public function __construct($config_id = 0) {
    // Load configuration set.
    $config_set = db_select('sonr_webmining_similar_widgets', 'sws')
      ->fields('sws')
      ->condition('id', $config_id)
      ->execute()
      ->fetch();

    if ($config_set) {
      $this->id = $config_id;
      $this->title = $config_set->title;
      $this->connection_id = $config_set->connection_id;
      $this->search_space_id = $config_set->search_space_id;
      $this->connection = SemanticConnector::getConnection('pp_server', $this->connection_id);

      // Add the config.
      $config = unserialize($config_set->config);
      $complete_config = self::getDefaultConfig();
      if (!is_null($config) && is_array($config)) {
        $complete_config = array_merge($complete_config, $config);
      }
      $this->config = $complete_config;
    }
    else {
      $this->id = 0;
      $this->title = '';
      $this->connection_id = 0;
      $this->search_space_id = '';
      $this->connection = SemanticConnector::getConnection('pp_server', $this->connection_id);
      $this->config = self::getDefaultConfig();
    }
  }

  /**
   * Getter-function for the PoolParty GraphSearch Similar ID.
   *
   * @return int
   *   ID of the configuration set
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter-function for the search_space_id-variable.
   *
   * @return string
   *   ID of the used PoolParty project.
   */
  public function getSearchSpaceId() {
    return $this->search_space_id;
  }

  /**
   * Getter-function for the title-variable.
   *
   * @return string
   *   Title of the configuration set
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server ID.
   *
   * @return int
   *   PoolParty GraphSearch server ID of the configuration set
   */
  public function getConnectionId() {
    return $this->connection_id;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server connection object.
   *
   * @return SemanticConnectorPPServerConnection
   *   PoolParty GraphSearch server connection object of the configuration set
   */
  public function getConnection() {
    return $this->connection;
  }

  /**
   * Getter-function for the config-variable.
   *
   * @return array
   *   Config of the configuration set
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Get the default config of a configuration set.
   *
   * @return array
   *   Config of the configuration set
   */
  public static function getDefaultConfig() {
    return array(
      'max_items' => 5,
    );
  }
}