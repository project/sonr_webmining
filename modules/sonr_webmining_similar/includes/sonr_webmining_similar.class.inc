<?php
/**
 * @file
 * The PoolParty GraphSearch SeeAlso class for displaying the
 * similar content.
 */

class SonrWebminingSimilarContent {

  /** @var \SonrWebminingSimilarConfig */
  protected $configuration_set;
  protected $config;
  /** @var \SemanticConnectorSonrApi $api */
  protected $api;

  public function __construct(SonrWebminingSimilarConfig $configuration_set) {
    $this->configuration_set = $configuration_set;
    $this->config = $configuration_set->getConfig();
    $this->api = $configuration_set->getConnection()->getApi('sonr');
  }

  /**
   * Gets similar documents from the node URL.
   *
   * @param string $url
   *   The URL of the node.
   *
   * @return array
   *   A list of links to documents.
   */
  public function fromUrl($url) {
    $similar_documents = array();

    // We need the search information to get the correct language.
    $connection_config = $this->configuration_set->getConnection()->getConfig();
    $graphsearch_config = $connection_config['sonr_configuration'];
    $searchspaces = semantic_connector_get_graphsearch_search_spaces($graphsearch_config, $this->configuration_set->getSearchSpaceId());

    $parameters = array(
      'count' => $this->config['max_items'],
      'locale' => $searchspaces[$this->configuration_set->getSearchSpaceId()]['language'],
    );
    $documents = $this->api->getSimilar($url, $this->configuration_set->getSearchSpaceId(), $parameters);
    if ($documents && !empty($documents['results'])) {
      foreach ($documents['results'] as $document) {
        $similar_documents[] = $this->createLink($document);
      }
    }

    return $similar_documents;
  }

  /**
   * Gets recommended documents from the PowerTagging tags of a node.
   *
   * @param object $node
   *   A node.
   *
   * @return array
   *   A list of links to documents.
   */
  public function fromTags($node) {
    $similar_documents = array();
    $fields = field_info_instances('node', $node->type);

    // Goes throw all fields and checks if a PowerTagging field exists.
    // If so, than gets all the term IDs from that field.
    $taxonomy_term_ids = array();
    foreach ($fields as $field => $instance) {
      if ($instance['widget']['module'] != 'powertagging' || empty($node->$field)) {
        continue;
      }
      $powertagging = powertagging_config_load($instance['settings']['powertagging_id']);
      $connection_config = $this->configuration_set->getConnection()->getConfig();
      if ($this->configuration_set->getConnectionId() == $powertagging->connection_id
        && isset($connection_config['sonr_configuration']['projects'][$powertagging->project_id])
        && isset($connection_config['sonr_configuration']['projects'][$powertagging->project_id]['search_spaces'][$this->configuration_set->getSearchSpaceId()])) {
        foreach ($node->{$field}[LANGUAGE_NONE] as $term_id) {
          $taxonomy_term_ids[] = $term_id['tid'];
        }
      }
    }

    // If terms found, than take the term labels as text for the recommendation.
    if (!empty($taxonomy_term_ids)) {
      $taxonomy_terms = taxonomy_term_load_multiple($taxonomy_term_ids);
      $taxonomy_term_names = array();
      foreach ($taxonomy_terms as $term) {
        $taxonomy_term_names[] = $term->name;
      }
      $text = implode(' ', $taxonomy_term_names);
      if (!empty($text)) {
        // We need the search information to get the correct language.
        $connection_config = $this->configuration_set->getConnection()->getConfig();
        $graphsearch_config = $connection_config['sonr_configuration'];
        $searchspaces = semantic_connector_get_graphsearch_search_spaces($graphsearch_config, $this->configuration_set->getSearchSpaceId());

        $parameters = array(
          'count' => $this->config['max_items'],
          'numberOfTerms' => 0,
          'numberOfConcepts' => count($taxonomy_terms),
          'locale' => $searchspaces[$this->configuration_set->getSearchSpaceId()]['language'],
        );
        $documents = $this->api->getSimilar($text, $this->configuration_set->getSearchSpaceId(), $parameters, 'text');
        if ($documents && !empty($documents['results'])) {
          foreach ($documents['results'] as $document) {
            $similar_documents[] = $this->createLink($document);
          }
        }
      }
    }

    return $similar_documents;
  }

  /**
   * Gets recommended documents from the content of a node.
   *
   * @param object $node
   *   A node.
   *
   * @return array
   *   A list of links to documents.
   */
  public function fromContent($node) {
    $similar_documents = array();

    // Gets the content of the node and take it for the recommendation.
    $node_content_array = node_view($node);
    $text = trim(strip_tags(drupal_render($node_content_array)));
    if (!empty($text)) {
      // We need the search information to get the correct language.
      $connection_config = $this->configuration_set->getConnection()->getConfig();
      $graphsearch_config = $connection_config['sonr_configuration'];
      $searchspaces = semantic_connector_get_graphsearch_search_spaces($graphsearch_config, $this->configuration_set->getSearchSpaceId());

      $parameters = array(
        'count' => $this->config['max_items'],
        'locale' => $searchspaces[$this->configuration_set->getSearchSpaceId()]['language'],
      );
      $documents = $this->api->getSimilar($text, $this->configuration_set->getSearchSpaceId(), $parameters, 'text');
      if ($documents && !empty($documents['results'])) {
        foreach ($documents['results'] as $document) {
          $similar_documents[] = $this->createLink($document);
        }
      }
    }

    return $similar_documents;
  }

  /**
   * Creates a hyperlink for a document.
   *
   * @param object $document
   *   A document.
   *
   * @return string
   *   The hiperlink to the document.
   */
  protected function createLink($document) {
    $attributes = array();
    if (strpos($document['link'], $GLOBALS['base_url']) === FALSE) {
      $attributes = array('target' => '_blank');
    }
    return l($document['title'], $document['link'], array('attributes' => $attributes));
  }
}