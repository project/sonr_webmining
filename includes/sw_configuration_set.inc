<?php

/**
 * @file
 * The class for PoolParty GraphSearch configuration set.
 */

class SonrWebminingConfigurationSet {

  protected $id;
  protected $search_space_id;
  protected $title;
  protected $connectionId;
  protected $connection;
  protected $config;

  /**
   * Constructor of the SonrWebminingConfigurationSet-class.
   *
   * @param int $config_id
   *   ID of the configuration set
   */
  public function __construct($config_id = 0) {
    // Load configuration set.
    $config_set = db_select('sonr_webmining_sets', 'sws')
      ->fields('sws')
      ->condition('swid', $config_id)
      ->execute()
      ->fetch();

    if ($config_set) {
      $this->id = $config_id;
      $this->search_space_id = $config_set->search_space_id;
      $this->title = $config_set->title;
      $this->connectionId = $config_set->connection_id;
      
      $connection_overrides = variable_get('sonr_webmining_override_connections', array());
      if (isset($connection_overrides[$config_id])) {
        $overrides = $connection_overrides[$config_id];
        if (isset($overrides['connection_id'])) {
          $this->connectionId = $overrides['connection_id'];
        }
        if (isset($overrides['search_space_id'])) {
          $this->search_space_id = $overrides['search_space_id'];
        }
        if (isset($overrides['title'])) {
          $this->title = $overrides['title'];
        }
      }
      
      $this->connection = SemanticConnector::getConnection('pp_server', $this->connectionId);

      // Add the config.
      $config = unserialize($config_set->config);
      $complete_config = self::getDefaultConfig();
      if (!is_null($config) && is_array($config)) {
        $complete_config = array_merge($complete_config, $config);
      }
      $this->config = $complete_config;
    }
    else {
      $this->id = 0;
      $this->search_space_id = '';
      $this->title = '';
      $this->connectionId = 0;
      $this->connection = SemanticConnector::getConnection('pp_server', $this->connectionId);
      $this->config = self::getDefaultConfig();
    }
  }

  /**
   * Getter-function for the PoolParty GraphSearch ID.
   *
   * @return int
   *   ID of the configuration set
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter-function for the search_space_id-variable.
   *
   * @return string
   *   ID of the used PoolParty project.
   */
  public function getSearchSpaceId() {
    return $this->search_space_id;
  }

  /**
   * Getter-function for the title-variable.
   *
   * @return string
   *   Title of the configuration set
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server ID.
   *
   * @return int
   *   PoolParty GraphSearch server ID of the configuration set
   */
  public function getConnectionId() {
    return $this->connectionId;
  }

  /**
   * Getter-function for the PoolParty GraphSearch server connection object.
   *
   * @return SemanticConnectorConnection
   *   PoolParty GraphSearch server connection object of the configuration set
   */
  public function getConnection() {
    return $this->connection;
  }

  /**
   * Getter-function for the config-variable.
   *
   * @return array
   *   Config of the configuration set
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Get the default config of a configuration set.
   *
   * @return array
   *   Config of the configuration set
   */
  public static function getDefaultConfig() {
    return array(
      // Result settings.
      'items_per_page' => 10,
      'show_results_count' => FALSE,
      'summary_max_chars' => 200,
      'date_format' => 'short',
      'link_target' => '_blank',
      'show_tags' => TRUE,
      'tags_max_items' => 10,
      'show_similar' => TRUE,
      'similar_max_items' => 5,
      'show_sentiment' => FALSE,
      'first_page_only' => FALSE,
      'cache_lifetime' => 600,
      'footer_id' => 'footer-wrapper',
      // Filter settings.
      'separate_blocks' => FALSE,
      'facets_to_show' => array(),
      'facet_max_items' => 10,
      'hide_empty_facet' => TRUE,
      'time_filter' => NULL,
      'time_filter_years' => NULL,
      'add_trends' => FALSE,
      'components_order' => array('facets', 'time', 'reset', 'trends'),
      // Search bar settings.
      'show_searchbar' => TRUE,
      'show_block_searchbar' => FALSE,
      'placeholder' => 'Search...',
      'ac_max_suggestions' => 10,
      'ac_min_chars' => 3,
      'ac_add_matching_label' => TRUE,
      'ac_add_context' => TRUE,
      'ac_add_facet_name' => FALSE,
      'show_facetbox' => TRUE,
      'search_type' => 'concept free-term',
      // Other settings.
      'use_css_file' => TRUE,
      'add_rss_functionality' => FALSE,
    );
  }
}
