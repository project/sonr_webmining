<?php
/**
 * @file
 * Callbacks and theme-functions used by PoolParty GraphSearch.
 */

/**
 * Get the results of the PoolParty GraphSearch via AJAX.
 *
 * @param int $configuration_set_id
 *   The ID from a PoolParty GraphSearch configuration set
 * @param int $page
 *   The page to start searching at
 *
 * @return string
 *   The rendered filters and results in a json format
 */
function sonr_webmining_get_results($configuration_set_id, $page) {
  $filters = isset($_GET['filters']) ? $_GET['filters'] : array();
  $aggregator = new SonrWebmining($configuration_set_id);
  $aggregator->setFilters($filters);
  $aggregator->search($page);

  $data = array(
    'list' => $aggregator->themeContent(),
    'filter' => $aggregator->themeFilters(),
  );

  print drupal_json_encode($data);

  exit();
}

/**
 * Get the RSS content by a given PoolParty GraphSearch configuration set ID.
 *
 * @param int $configuration_set_id
 *   The ID from a PoolParty GraphSearch configuration set
 *
 * @return string
 *   The RSS content
 */
function sonr_webmining_get_rss($configuration_set_id) {
  // Check if the configuration set exists
  $aggregator = new SonrWebmining($configuration_set_id);
  if ($aggregator->getId() <= 0) {
    drupal_set_message(t('The PoolParty GraphSearch configuration ID %id does not exists.', array('%id' => $configuration_set_id)), 'error');
    drupal_goto('<front>');
  }

  // There are filters, so look for an existing short-URL.
  if (isset($_GET['filters'])) {
    $filter_string = drupal_http_build_query(array('filters' => $_GET['filters']));

    // Check if there is a short URL available for this set of filters and
    // this configuration set id.
    $shorturl = db_select('sonr_webmining_rss_shorturls', 'su')
      ->fields('su', array('short_url'))
      ->condition('swid', $configuration_set_id)
      ->condition('filter_string', $filter_string)
      ->execute()
      ->fetchField();

    if ($shorturl === FALSE) {
      // Create a new short URL for the filter-combination.
      $rsssuid = db_insert('sonr_webmining_rss_shorturls')
        ->fields(array(
          'swid' => $configuration_set_id,
          'filter_string' => $filter_string,
        ))
        ->execute();

      $shorturl = sonr_webmining_create_shorturl($rsssuid);

      // Update the database with the created shorturl.
      db_update('sonr_webmining_rss_shorturls')
        ->fields(array(
          'short_url' => $shorturl,
        ))
        ->condition('swid', $configuration_set_id)
        ->condition('filter_string', $filter_string)
        ->execute();
    }

    // Redirect to the short URL.
    drupal_goto('sonr-webmining/rss/' . $shorturl);
  }
  else {
    $aggregator->search();
    print $aggregator->themeContent('rss');
  }

  exit();
}

/**
 * Get the RSS content by a given short URL.
 *
 * @param string $shorturl
 *   The short URL
 *
 * @return string
 *   The RSS content
 */
function sonr_webmining_get_rss_by_shorturl($shorturl) {
  // Get the configuration set id and the filters from the database.
  $shorturl_infos = db_select('sonr_webmining_rss_shorturls', 'su')
    ->fields('su', array('swid', 'filter_string'))
    ->condition('short_url', $shorturl)
    ->execute()
    ->fetch();

  // If the shorturl exists, set the filters and display the content.
  if ($shorturl_infos !== FALSE) {
    $aggregator = new SonrWebmining($shorturl_infos->swid);
    parse_str(parse_url('?' . $shorturl_infos->filter_string, PHP_URL_QUERY), $filter_array);
    $aggregator->setFilters($filter_array['filters']);
    $aggregator->search();
    print $aggregator->themeContent('rss');
  }

  exit();
}

/**
 * Get similar documents from a given document via AJAX.
 *
 * @param int $configuration_set_id
 *   The ID from a PoolParty GraphSearch configuration set
 *
 * @return string
 *   The rendered content
 */
function sonr_webmining_get_similars($configuration_set_id) {
  if (isset($_GET['uri'])) {
    $aggregator = new SonrWebmining($configuration_set_id);
    $aggregator->searchSimilars($_GET['uri']);
    print $aggregator->themeSimilars();
  }
  exit();
}

/**
 * Get the suggested concepts for the autocomplete field in the search bar.
 *
 * @param int $configuration_set_id
 *   The ID from a PoolParty GraphSearch configuration set
 * @param int $max_items
 *   The maximum number of concepts displayed in the drop down list
 * @param string $facet_id
 *   The ID of the facet to filter the suggestions for
 *
 * @return string
 *   The list of found concepts in a json format
 */
function sonr_webmining_autocomplete($configuration_set_id, $max_items, $facet_id = "") {
  $matches = array();
  $term = isset($_GET['term']) ? $_GET['term'] : '';
  if (empty($term)) {
    drupal_json_output($matches);
    exit();
  }

  $configuration_set = sonr_webmining_config_load($configuration_set_id);
  // We need the search information to get the correct language.
  $connection_config = $configuration_set->getConnection()->getConfig();
  $graphsearch_config = $connection_config['sonr_configuration'];
  $searchspaces = semantic_connector_get_graphsearch_search_spaces($graphsearch_config, $configuration_set->getSearchSpaceId());

  /** @var \SemanticConnectorSonrApi $sonr_api */
  $sonr_api = $configuration_set->getConnection()->getApi('sonr');
  $parameters = array(
    'count' => $max_items,
    'locale' => $searchspaces[$configuration_set->getSearchSpaceId()]['language'],
  );
  if (!empty($facet_id)) {
    $parameters['context'] = $facet_id;
  }

  if (($result = $sonr_api->suggest($term, $configuration_set->getSearchSpaceId(), $parameters)) !== FALSE) {
    $configuration_set_config = $configuration_set->getConfig();
    $facet_labels =  [];
    // Get the facet labels.
    if ($configuration_set_config['ac_add_context'] && $configuration_set_config['ac_add_facet_name']) {
      $sonr_webmining = new SonrWebmining($configuration_set_id);
      $facet_labels = $sonr_webmining->getAllFacets();

      // Overwrite the facet labels with custom ones if available.
      foreach ($configuration_set_config['facets_to_show'] as $facet) {
        if (!empty($facet['name'])) {
          $facet_labels[$facet['facet_id']] = $facet['name'];
        }
      }
    }
    foreach ($result['results'] as $data) {
      $matches[] = array(
        'label' => $data['label'],
        'value' => $data['id'],
        'field' => $data['field'],
        'matching_label' => $data['matchingLabel'],
        'context' => $data['context'] . ($configuration_set_config['ac_add_facet_name'] && isset($facet_labels[$data['field']]) ? ' (' . $facet_labels[$data['field']] . ')' : ''),
      );
    }
  }

  drupal_json_output($matches);
  exit();
}

/**
 * Create a shorturl out of an integer.
 *
 * @param int $rsssuid
 *   The ID of the RSS short URL
 *
 * @return string
 *   The short URL
 */
function sonr_webmining_create_shorturl($rsssuid) {
  $rsssuid--;

  $chars_to_use = range('a', 'z');
  $chars_to_fill = range('A', 'Z');
  $length = count($chars_to_use);
  $code = '';

  // Calculate the new shorturl.
  while ($rsssuid > $length - 1) {
    // Determine the value of the next higher character.
    $code = $chars_to_use[(int) fmod((float) $rsssuid, (float) $length)] . $code;
    // Reset $rsssuid to remaining value to be converted.
    $rsssuid = floor($rsssuid / $length) - 1;
  }

  // Remaining value of $id is less than the length of $chars_to_use.
  $code = $chars_to_use[$rsssuid] . $code;

  // Add fill-characters to always have a string of 6 chars length.
  $fill_string = "";
  $max = count($chars_to_fill) - 1;
  for ($i = 0; $i < (6 - strlen($code)); $i++) {
    $rand = mt_rand(0, $max);
    $fill_string .= $chars_to_fill[$rand];
  }

  return $code . $fill_string;
}

/**
 * Saves the selected filters for the logged in user.
 *
 * @param int $configuration_set_id
 *   The PoolParty GraphSearch configuration ID.
 *
 * @return string
 *   "saved" or an error if not saved.
 */
function sonr_webmining_save_filter($configuration_set_id) {
  global $user;

  $errors = array();
  if (!isset($_GET['filters']) || empty($_GET['filters'])) {
    $errors[] = t('The filter is empty. Please select your preferred filters.');
  }
  if (!isset($_GET['title']) || empty($_GET['title'])) {
    $errors[] =  t('No title is given. Please add a descriptive title.');
  }
  if (empty($errors)) {
    $filter = drupal_http_build_query(array('filters' => $_GET['filters']));
    $result = db_insert('sonr_webmining_search_filters')
      ->fields(array(
        'uid' => $user->uid,
        'swid' => $configuration_set_id,
        'filter_string' => $filter,
        'title' => $_GET['title'],
        'time_interval' => $_GET['time_interval'],
        'timestamp' => time(),
      ))
      ->execute();
    echo $result ? 'saved' : t('Error: Filter can not be saved.');
  }
  else {
    echo implode('<br />', $errors);
  }
  exit();
}

/**
 * Returns all the saved filters from the logged in user.
 *
 * @param int $configuration_set_id
 *   The PoolParty GraphSearch configuration ID.
 *
 * @return string
 *   The saved filters as a HTML list.
 */
function sonr_webmining_get_filters($configuration_set_id) {
  global $user;

  $result = db_select('sonr_webmining_search_filters', 'sf')
    ->fields('sf', array('sfid', 'title', 'filter_string', 'time_interval', 'timestamp'))
    ->condition('uid', $user->uid)
    ->condition('swid', $configuration_set_id)
    ->orderBy('timestamp')
    ->execute();

  $sonr_config = sonr_webmining_config_load($configuration_set_id);
  $settings = $sonr_config->getConfig();

  $items = array();
  while ($record = $result->fetchAssoc()) {
    parse_str(parse_url('?' . $record['filter_string'], PHP_URL_QUERY), $filter);
    $filter = drupal_json_encode($filter);
    $item ='<a class="search-filter-title" data-filter=\'' . $filter . '\'>' . $record['title'] . '</a>';
    if ($record['time_interval']) {
      $item .= '<div class="search-filter-time-interval">(' . $record['time_interval'] . ' ' . t('e-mail alert') . ')</div>';
    }
    $item .= '<div class="search-filter-timestamp">' . format_date($record['timestamp'], (isset($settings['date_format']) ? $settings['date_format'] : 'short')) . '</div>';
    $item .= '<a class="search-filter-remove" data-sfid="' . $record['sfid'] . '">' . t('remove') . '</a>';
    $items[] = $item;
  }
  if (!empty($items)) {
    echo theme('item_list', array('items' => $items, 'type' => 'ul'));
  }
  else {
    echo t('You do not have stored filters until now.');
  }
  exit();
}

/**
 * Deletes a saved filter from the logged in user.
 *
 * @param int $configuration_set_id
 *   The PoolParty GraphSearch configuration ID.
 *
 * @return string
 *   "saved" or an error if not saved.
 */
function sonr_webmining_delete_filter($configuration_set_id) {
  global $user;

  $result = db_delete('sonr_webmining_search_filters')
    ->condition('sfid', $_GET['sfid'])
    ->condition('uid', $user->uid)
    ->condition('swid', $configuration_set_id)
    ->execute();

  echo $result ? 'deleted' : t("Error: Filter can not be removed");
  exit();
}

