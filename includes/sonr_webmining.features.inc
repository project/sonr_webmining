<?php

/**
 * @file
 * All hooks required to export and import PoolParty GraphSearch configurations
 * with the Features module.
 */

/**
 * Implementation of hook_features_export_options()
 */
function sonr_webmining_config_features_export_options() {
  $options = array();
  $sonr_webmining_configs = sonr_webmining_config_load_multiple();
  foreach ($sonr_webmining_configs as $sonr_webmining_config) {
    $options[$sonr_webmining_config->getId()] = $sonr_webmining_config->getTitle();
  }
  return $options;
}

/**
 * Implementation of hook_features_export()
 */
function sonr_webmining_config_features_export($data, &$export, $module_name = '') {
  $export['dependencies'][] = 'sonr_webmining';

  foreach ((array) $data as $swid) {
    $export['features']['sonr_webmining_config'][$swid] = $swid;
  }

  return array();
}

/**
 * Implementation of hook_features_export_render()
 */
function sonr_webmining_config_features_export_render($module_name, $data) {
  $sonr_webmining_conf_array = array();
  foreach ($data as $swid) {
    $sonr_webmining_config = sonr_webmining_config_load($swid);

    if (!empty($sonr_webmining_config)) {
      // Create a simplified array of the PoolParty GraphSearch configuration
      // set object to make savable in the feature.
      $pp_server_connection = $sonr_webmining_config->getConnection();
      $sonr_webmining_config_simple = array(
        'swid' => $swid,
        'title' => $sonr_webmining_config->getTitle(),
        'connection_id' => $pp_server_connection->getId(),
        'search_space_id' => $sonr_webmining_config->getSearchSpaceId(),
        'config' => $sonr_webmining_config->getConfig(),
        'connection_details' => array(
          'title' => $pp_server_connection->getTitle(),
          'url' => $pp_server_connection->getUrl(),
          'credentials' => $pp_server_connection->getCredentials(),
        ),
      );

      $sonr_webmining_conf_array[$swid] = $sonr_webmining_config_simple;
    }
  }
  $code = '  $data = ' . features_var_export($sonr_webmining_conf_array, '  ') . ';' . PHP_EOL;
  $code .= '  return $data;';

  return array('sonr_webmining_config_features_settings' => $code);
}

/**
 * Implementation of hook_features_rebuild()
 */
function sonr_webmining_config_features_rebuild($module) {
  sonr_webmining_config_features_revert($module);
}

/**
 * Implementation of hook_features_revert()
 */
function sonr_webmining_config_features_revert($module) {
  if ($data = features_get_default('sonr_webmining_config', $module)) {
    foreach ($data as $swid => $sonr_webmining_config) {
      // Check if the connection already exists, otherwise create it.
      $potential_connections = SemanticConnector::searchConnections(array(
        'type' => 'pp_server',
        'url' => $sonr_webmining_config['connection_details']['url'],
      ));

      // Connection already exists, use the first one found.
      if (!empty($potential_connections)) {
        $connection = reset($potential_connections);
      }
      // Connection doesn't exist yet, create it.
      else {
        $connection = SemanticConnector::createConnection('pp_server', $sonr_webmining_config['connection_details']['url'], $sonr_webmining_config['connection_details']['title'], $sonr_webmining_config['connection_details']['credentials']);
      }

      // Restore the PoolParty GraphSearch configurations.
      db_query("DELETE FROM {sonr_webmining_sets} WHERE swid = :swid", array(':swid' => $swid));
      db_insert('sonr_webmining_sets')
        ->fields(array(
          'swid' => $swid,
          'title' => $sonr_webmining_config['title'],
          'connection_id' => $connection->getId(),
          'search_space_id' => $sonr_webmining_config['search_space_id'],
          'config' => serialize($sonr_webmining_config['config']),
        ))
        ->execute();
    }
  }
}
